import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxPaginationModule } from 'ngx-pagination';

import { SwiperModule } from "swiper/angular";
import { LoginComponent } from './login/login.component';
import { CKEditorModule } from 'ckeditor4-angular';
import { AdminLayoutComponent } from './admin/admin-layout/admin-layout.component';
import { CategoryComponent } from './admin/category/category.component';
import { OrderComponent } from './admin/order/order.component';
import { CommentComponent } from './admin/comment/comment.component';
import { SalecodeComponent } from './admin/salecode/salecode.component';
import { CustomerComponent } from './admin/customer/customer.component';
import { PaymentComponent } from './admin/payment/payment.component';
import { ProductDetailComponent } from './admin/product/product-detail/product-detail.component';
import { SalecodeCreateComponent } from './admin/salecode/salecode-create/salecode-create.component';
import { SalecodeUpdateComponent } from './admin/salecode/salecode-update/salecode-update.component';
import { ProductComponent } from './admin/product/product.component';
import { ProductUpdateComponent } from './admin/product/product-update/product-update.component';
import { ProductCreateComponent } from './admin/product/product-create/product-create.component';
import { OrderUpdateComponent } from './admin/order/order-update/order-update.component';
import { OrderCreateComponent } from './admin/order/order-create/order-create.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { SideNavAdminComponent } from './admin/common/component/side-nav-admin/side-nav-admin.component';
import { TableComponent } from './admin/common/component/table/table.component';
import { NavMenuComponent } from './admin/common/component/nav-menu/nav-menu.component';
import { CategoryUpdateComponent } from './admin/category/category-update/category-update.component';
import { CategoryCreateComponent } from './admin/category/category-create/category-create.component';
import { CommonModule, DatePipe } from '@angular/common';
import { ChartsModule } from 'ng2-charts';
import { ErrorComponent } from './error/error.component';
import { PipeHostImagePipe } from './admin/common/pipeline/image-convert.pipeline';
import { CacheInterceptor } from './admin/common/interceptors/cacheimtercept';
import { AccordionModule } from 'ngx-bootstrap/accordion';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminLayoutComponent,
    CategoryComponent,
    OrderComponent,
    CommentComponent,
    SalecodeComponent,
    CustomerComponent,
    PaymentComponent,
    CategoryCreateComponent,
    CategoryUpdateComponent,
    NavMenuComponent,
    TableComponent,
    SideNavAdminComponent,
    DashboardComponent,
    OrderCreateComponent,
    OrderUpdateComponent,
    SalecodeCreateComponent,
    SalecodeUpdateComponent,
    ProductComponent,
    ProductCreateComponent,
    ProductDetailComponent,
    ProductUpdateComponent,
    ErrorComponent,
    PipeHostImagePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    NgxPaginationModule,
    SwiperModule,
    FormsModule,
    ReactiveFormsModule,
    CKEditorModule,
    ChartsModule,
    AccordionModule.forRoot()

  ],
  providers: [
    DatePipe,
    { provide: HTTP_INTERCEPTORS, useClass: CacheInterceptor, multi: true } 
   
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }

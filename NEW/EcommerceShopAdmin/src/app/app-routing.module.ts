import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminLayoutComponent } from './admin/admin-layout/admin-layout.component';
import { CommentComponent } from './admin/comment/comment.component';
import { CustomerComponent } from './admin/customer/customer.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { OrderComponent } from './admin/order/order.component';
import { ProductCreateComponent } from './admin/product/product-create/product-create.component';
import { ProductDetailComponent } from './admin/product/product-detail/product-detail.component';
import { ProductUpdateComponent } from './admin/product/product-update/product-update.component';
import { ProductComponent } from './admin/product/product.component';
import { SalecodeCreateComponent } from './admin/salecode/salecode-create/salecode-create.component';
import { SalecodeUpdateComponent } from './admin/salecode/salecode-update/salecode-update.component';
import { SalecodeComponent } from './admin/salecode/salecode.component';
import { ErrorComponent } from './error/error.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  // { path:'',component:LoginComponent,children:[
  //   { path: 'admin',component: AdminLayoutComponent, children: [
  //     {path:'dashboard',component:DashboardComponent},
  //     {path:'order',component:OrderComponent},
  //     {path:'product',children:[
  //       {path:'',component:ProductComponent},
  //       {path:'create',component:ProductCreateComponent},
  //       {path:'update/:id',component:ProductUpdateComponent},
  //       {path:'detail/:id',component:ProductDetailComponent}
  //     ]},
  //     {path:'customer',component:CustomerComponent},
  //     {path:'comment',component:CommentComponent},
  //     {path:'salecode',children:[
  //       {path:'',component:SalecodeComponent},
  //       {path:'create',component:SalecodeCreateComponent},
  //       {path:'update',component:SalecodeUpdateComponent}
  //     ]},
  
  //   ]},
  //   {
  //     path:'error',component:ErrorComponent
  //   }
  // ]}
  {path:'',  redirectTo: 'login', pathMatch: 'full'},
  { path: 'admin',
  component: AdminLayoutComponent, children: [
    {path:'dashboard',component:DashboardComponent},
    {path:'order',component:OrderComponent},
    {path:'product',children:[
      {path:'',component:ProductComponent},
      {path:'create',component:ProductCreateComponent},
      {path:'update/:id',component:ProductUpdateComponent},
      {path:'detail/:id',component:ProductDetailComponent}
    ]},
    
    {path:'customer',component:CustomerComponent},
    {path:'comment',component:CommentComponent},
    {path:'salecode' ,children:[
      {path:'',component:SalecodeComponent},
      {path:'create',component:SalecodeCreateComponent},
      {path:'update',component:SalecodeUpdateComponent}
    ]},

  ]},
  {
    path:'login',component:LoginComponent
  },
  {
    path:'error',component:ErrorComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DrashbroadAdminService {
  private apiUrl = environment.apiUrl;
  constructor(private httpClient : HttpClient) { }
  getBenefitByMonthOrYear(option : any):Observable<HttpResponse<any[]>>{
    return this.httpClient.get<any[]>(this.apiUrl+`Benefit/GetBenefit?option=${option}`,{observe: 'response'});
  } 
}

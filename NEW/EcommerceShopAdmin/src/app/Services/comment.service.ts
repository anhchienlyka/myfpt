import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  private apiUrl = environment.apiUrl;
  constructor(private httpClient: HttpClient) { }
  getAllComments():Observable<HttpResponse<any>>{
    var options = {
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      responseType:'text' as const
    };
    return this.httpClient.get<any>(this.apiUrl+'Comment/GetAllComment',{observe: 'response' });
  }
GetCoutCommentByProductId(id:any):Observable<HttpResponse<any>>{
    var options = {
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      responseType:'text' as const
    };
    return this.httpClient.get<any>(this.apiUrl+`Comment/GetCoutCommentByProductId?id=${id}`,{observe: 'response' });
  }
}

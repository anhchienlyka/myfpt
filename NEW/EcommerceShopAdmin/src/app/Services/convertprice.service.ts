import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConvertpriceService {

  constructor() { }
  convertVnd(price:any){
    price =price.toString();
    let priceOut="";
    let cout = 0;
    for (let i = price.length-1;i>=0;i--)
    {
        if (cout==3)
        {
            priceOut = priceOut + ".";
            cout = 0;
        }
        priceOut = priceOut + price[i];
        cout++;
    }
    let result = priceOut.split("").reverse().join("");
    result = result + " VNĐ";
    return result;
  }

}

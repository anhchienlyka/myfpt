import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
import { Order } from '../model/order.model';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private apiUrl = environment.apiUrl;
  constructor(private httpClient: HttpClient) { }
//https://localhost:44311/api/Category/GetAllCategory
  getOrders():Observable<HttpResponse<any>>{
    var options = {
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      responseType:'text' as const
    };
    return this.httpClient.get<any>(this.apiUrl+'Order/GetAllOrder',{observe: 'response' });
  }


  updateStatusOrder(data:any):Observable<HttpResponse<any>>{
    return this.httpClient.post(this.apiUrl+'Order/UpdateStatusOrder',data, {observe: 'response' })
  }
  getRevenueStatisticByDay():Observable<HttpResponse<any>>{
    return this.httpClient.get(this.apiUrl+'Order/GetRevenueStatisticByDay', {observe: 'response' })
  }
  getTopProductByMonth():Observable<HttpResponse<any>>{
    return this.httpClient.get(this.apiUrl+'Order/GetTopProductByMonth', {observe: 'response' })
  
  }
}

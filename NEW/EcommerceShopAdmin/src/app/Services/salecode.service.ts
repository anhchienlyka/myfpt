import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
import { Salecode } from '../model/salecode';

@Injectable({
  providedIn: 'root'
})
export class SalecodeService {

  private apiUrl = environment.apiUrl;
  constructor(private httpClient: HttpClient) { }

//https://localhost:44311/api/SaleCode/GetCodeByCodeName?name=d
  getSaleCodeByCodeName (codeName:string){
    var url = this.apiUrl + `SaleCode/GetCodeByCodeName?name=${codeName}`;
    return this.httpClient.get<Salecode[]>(url, {observe: 'response'});
  }
  getAllSaleCode():Observable<HttpResponse<any>>{
    var options = {
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      responseType:'text' as const
    };
    return this.httpClient.get<any>(this.apiUrl+'SaleCode/GetSaleCodeName',{observe: 'response' });
  }

  addSaleCode(data :any):Observable<HttpResponse<any>>{
    var test = this.apiUrl+`SaleCode/AddSaleCode`;
    return this.httpClient.post<any>(this.apiUrl+`SaleCode/AddSaleCode`,data,{observe:'response'})
  }
  deleteSaleCode(id :any):Observable<HttpResponse<any>>{
    return this.httpClient.delete<any>(this.apiUrl+`SaleCode/DeleteCode?id=${id}`,{observe:'response'})
  }

  updateSaleCode(data :any):Observable<HttpResponse<any>>{
    return this.httpClient.post<any>(this.apiUrl+'SaleCode/UpdateSaleCode',data,{observe:'response'})
  }
}

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConvertimageService {

  constructor() { }
  
  createImgPath = (serverPath: string)=>{

    return `https://localhost:44311/${serverPath}`;

  }
}

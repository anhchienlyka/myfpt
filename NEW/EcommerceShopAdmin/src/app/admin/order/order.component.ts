import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { OrderStatusUpdate } from 'src/app/model/order.model';
import { NotificationService } from 'src/app/notification/notification.service';
import { AccountService } from 'src/app/Services/account.service';
import { OrderService } from 'src/app/Services/order.service';
import { Options } from '../common/interfaces/options';


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css'],
})
export class OrderComponent implements OnInit {

  pageIndex: number=1;
  pageSize: number=2;
  totalPages: number;
  p : number = 1;


  Options: Options;
  Headers: any[];
  dataTables: any = [];
  orderStatus: any = {};
  idAdmin:any
  constructor(
    private _orderService: OrderService,
    private ref: ChangeDetectorRef,
    private datepipe: DatePipe,
    private _notificationService: NotificationService,
    private accountService:AccountService
  ) {}

  ngOnInit(): void {
    this.initGenerate();
    this.getCurrentUer();
  }

  async initGenerate() {
    this.Options = {
      pagging: true,
      paggingNumber: [10, 20, 50, 100],
      selectAll: true,
      searchField: true,
      title: 'Danh sách đơn hàng',
    };
    this.Headers = [
      'Mã đơn hàng',
      'Khác hàng',
      'Ngày tạo',
      'Giá',
      'Trạng thái',
      'Thanh toán',
      'Cập nhật',
    ];
    this._orderService.getOrders().subscribe((item) => {
      let data1 = item.body.data.map((item) => {
        return {
          id: item.id,
          orderCode: item.orderCode,
          fullName: item.fullName,
          userId: item.userId,
          createDate: this.datepipe.transform(item.createDate, 'dd-MM-yyyy hh:mm'),
          totalCost: item.totalCost,
          transacStatus: item.transacStatus,
          paymentId: item.paymentId,
        };
      });
      this.dataTables = data1;
      this.ref.detectChanges();
    });
 
  }

  getCurrentUer(){
    var user =this.accountService.getCurrentUser();
    this.idAdmin = user.id;
  }
  updateStatus(id: any, status: any) {
    let orderStatus = {
      id: id,
      userId: this.idAdmin,
      transacStatus: status,
    };
    this._orderService.updateStatusOrder(orderStatus).subscribe((res) => {  
      this.initGenerate();
     if(res.body.data!=3){
      this._notificationService.showSuccess("Cập nhật trạng thái thành công", 'Thông báo');
     }
    });
  }
}

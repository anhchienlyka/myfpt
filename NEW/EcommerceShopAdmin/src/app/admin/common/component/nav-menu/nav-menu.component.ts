import { Component, OnInit, Input} from '@angular/core';
import { NavItem } from '../../interfaces/nav-item';



@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {
  @Input() public navItems: NavItem[];
  constructor() { }

  ngOnInit(): void {

  }
  getRouterLink(url: string): object {
    return { outlets: { primary: url, first: url, second: url } };
}

}

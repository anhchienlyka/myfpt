import { Component, OnInit } from '@angular/core';
import { MenuObjectJson } from '../../menu-json';

@Component({
  selector: 'app-side-nav-admin',
  templateUrl: './side-nav-admin.component.html', 
  styleUrls: ['./side-nav-admin.component.css']
})
export class SideNavAdminComponent implements OnInit {

  constructor() {
    this.onInitMenu();
   }
  public navItems: any

  ngOnInit(): void {
   
  }

  onInitMenu(){
    this.navItems = MenuObjectJson.getObjctMenu();
    console.log("this.navItems", this.navItems);
  }

}

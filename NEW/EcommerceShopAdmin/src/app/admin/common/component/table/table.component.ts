import { KeyValue } from '@angular/common';
import { ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { EventEmitter } from 'events';
import { Subject } from 'rxjs';
import { Options } from '../../interfaces/options';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit, OnDestroy, OnChanges {
  @Input() public Headers: any[];
  @Input() public Options: Options; 
  @Input() public DataTables: any[];
  @Output() listDataSelect = new EventEmitter();
  @Output() public click = new EventEmitter();
  private readonly destroy$ = new Subject<void>();
  numberPagging: number = 10;
  dataTables: any[];
  numberPaggingShow: any;
  selected :any = 1;
  textSearch: any;
  showBtnNext: boolean = false;
  showBtnPrevious: boolean = true;
  constructor( private ref: ChangeDetectorRef) {
     
   }
  ngOnChanges(changes: SimpleChanges): void {
    this.dataTables = this.DataTables.slice(this.selected - 1, this.numberPagging);
    this.initGenerate(this.DataTables);
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  ngOnInit(): void {
    console.log("this.DataTables", this.DataTables)
    this.initGenerate(this.DataTables);
  }
  originalOrder = (a: KeyValue<number,string>, b: KeyValue<number,string>): number => {
    return 0;
  };
  initGenerate(data){
    console.log("this.DataTables.lengt",data.length)
    
    let a =data.length % this.numberPagging;
    let b = (data.length - a)/this.numberPagging;
    if(data.length % this.numberPagging !== 0){
      this.numberPaggingShow = this.counter(b + 1);
      
    }
    else{
      this.numberPaggingShow = this.counter(b);
    }
  };
  handleSearchField($event){
    this.textSearch = $event.target.value;
    this.handleSearchFuncCommon(this.textSearch);
  };
  haddlePadding(item){
    console.log("item", item);
    this.selected = item;
    this.handleSearchFuncCommon(this.textSearch);
  };
  isActive(item) {
    return this.selected === item;
  };
  counter(i: number) {
    return new Array(i);
  }
  handleSearchFuncCommon(textSearch){
    debugger
    let data1 = [];
    if(textSearch){
      data1 = this.DataTables.filter((data)=>{
        return data.name.includes(textSearch)
      })
      this.initGenerate(data1);
      this.dataTables = data1.slice((this.selected - 1) * this.numberPagging  , this.selected * this.numberPagging);
    }
    else{
      this.initGenerate(this.DataTables);
      this.dataTables = this.DataTables.slice((this.selected - 1) * this.numberPagging  , this.selected * this.numberPagging);
    }
  }
  onChange(item){
    this.numberPagging = item;
    this.initGenerate(this.DataTables);
    this.dataTables = this.DataTables.slice(this.selected - 1, this.numberPagging);
  }
  haddlePaddingNext(event){
    this.selected++; 
    this.handleSearchFuncCommon(this.textSearch);
    if( this.numberPaggingShow.length >= this.selected + 1    ) { 
      this.showBtnNext = false; this.showBtnPrevious = true;  
      }
    else{ this.showBtnNext = true; this.showBtnPrevious = false; event.preventDefault(); return;}
    
  }
  haddlePaddingPrevious(event){
    this.selected--;
    this.handleSearchFuncCommon(this.textSearch);
    if( this.selected > 1  ) { ; 
      this.showBtnNext = true; this.showBtnPrevious = false; }
    else{ this.showBtnNext = false; this.showBtnPrevious = true; event.preventDefault(); return;}
    
  }
  selectRowTable(row){
    console.log("row",row);
  }

}

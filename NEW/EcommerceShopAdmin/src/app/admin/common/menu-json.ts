import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";

const objectMenu =  [
    {
        title: "Dashboard",
        url: "/admin/dashboard",
        target: "_blank",
        icon: "anticon-dashboard"
    },
    {
      title: "Product",
          url: "/admin/product-list",
          target: "_blank",
          icon: "anticon-dashboard"
      },
      {
          title: "Order",
          url: "/admin/order-list",
          active: true,
          icon: "anticon-dashboard",
        
      },
      {
        title: "Khách hàng",
        url: "/admin/customer-list",
        active: true,
        icon: "anticon-dashboard",
      
      },
      {
        title: "Mã giảm giá",
        url: "/admin/sale-list",
        active: true,
        icon: "anticon-dashboard",
      
      },
      
  ]
  export class MenuObjectJson {
    public static getObjctMenu(){
        return objectMenu;
    }
}
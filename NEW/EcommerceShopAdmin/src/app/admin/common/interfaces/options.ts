export interface Options {
    selectAll?: boolean;
    searchField?: boolean;
    pagging?: boolean;
    paggingNumber: number[] ;
    action?: boolean ;
    sort?: boolean;
    title?: string;
  }
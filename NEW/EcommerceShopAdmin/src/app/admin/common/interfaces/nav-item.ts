export interface NavItem {
    title: string;
    active: boolean;
    target: string;
    icon: string;
    url: string;
    children: NavItem[];
  }
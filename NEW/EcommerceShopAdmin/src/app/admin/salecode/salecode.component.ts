import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/notification/notification.service';
import { SalecodeService } from 'src/app/Services/salecode.service';

@Component({
  selector: 'app-salecode',
  templateUrl: './salecode.component.html',
  styleUrls: ['./salecode.component.css'],
})
export class SalecodeComponent implements OnInit {
  Headers: any[];
  dataTables: any = [];
  constructor(
    private salecodeService: SalecodeService,
    private ref: ChangeDetectorRef,
    private datepipe: DatePipe,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.initGenerate();
  }
  async initGenerate() {
    this.salecodeService.getAllSaleCode().subscribe((item) => {
      let data1 = item.body.data.map((item) => {
        return {
          id: item.id,
          codeName: item.codeName,
          valueCode: item.valueCode,
          startDateCode: this.datepipe.transform(
            item.startDateCode,
            'dd-MM-yyyy'
          ),
          endDateCode: this.datepipe.transform(item.endDateCode, 'dd-MM-yyyy'),
          isDelete: item.isDelete,
        };
      });
      this.dataTables = data1;
    });
  }
  deleteSaleCode(id: any) {
    this.salecodeService.deleteSaleCode(id).subscribe((res) => {
     this.notificationService.showSuccess("Xóa mã giảm giá thành công","Thông báo")
    });
  }
}

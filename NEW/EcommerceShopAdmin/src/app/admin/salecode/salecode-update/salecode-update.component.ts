import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationService } from 'src/app/notification/notification.service';
import { SalecodeService } from 'src/app/Services/salecode.service';

@Component({
  selector: 'app-salecode-update',
  templateUrl: './salecode-update.component.html',
  styleUrls: ['./salecode-update.component.css']
})
export class SalecodeUpdateComponent implements OnInit {

  constructor(
    private _salecodeService: SalecodeService,
    private _notificationService: NotificationService,
    private ref: ChangeDetectorRef,
    private spinner: NgxSpinnerService,
    private router:Router,
    public fb: FormBuilder,
  ) { }

  ngOnInit(): void {
  }


  myForm = this.fb.group({
    codeName: new FormControl('', [Validators.required]),
    valueCode: new FormControl('', [Validators.required]),
    isDelete: new FormControl('', [Validators.required]),
    startDateCode: new FormControl('', [Validators.required]),
    endDateCode:new FormControl('',[Validators.required] )
  });



  onSubmit(){
    let payload = {
      ...this.myForm.value
    };
    this._salecodeService.addSaleCode(payload).subscribe((item) => {
      this._notificationService.showSuccess("Thêm mã giảm giá công", 'Thông báo');
      this.router.navigateByUrl('admin/salecode')
    });
  }
}

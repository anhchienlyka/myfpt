import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subject } from 'rxjs/internal/Subject';
import { NotificationService } from 'src/app/notification/notification.service';
import { ConvertimageService } from 'src/app/Services/convertimage.service';
import { ConvertpriceService } from 'src/app/Services/convertprice.service';
import { ProductService } from 'src/app/Services/product.service';
import { Options } from '../common/interfaces/options';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
})
export class ProductComponent implements OnInit {
  pageIndex: number = 1;
  pageSize: number = 2;
  totalPages: number;
  p: number = 1;

  Options: Options;
  Headers: any[];
  keywords: any[];
  dataTables: any = [];
  createProductModal: string = '';
  ngUnsubscribe: Subject<void> = new Subject<void>();
  itemSelected: any;

  constructor(
    private _productService: ProductService,
    private ref: ChangeDetectorRef,
    private _notificationService: NotificationService,
    private spinner: NgxSpinnerService,
    private convertVnd: ConvertpriceService,
    private convertimage: ConvertimageService
  ) {}

  ngOnInit(): void {
    this.initGenerate();
  }
  async initGenerate() {
    this.spinner.show();
    await this._productService.getAllProducts().subscribe((item) => {
      let data1 = item.body.data.map((item) => {
        return {
          id: item.id,
          name: item.name,
          price: this.convertVnd.convertVnd(item.price),
          priceInput: this.convertVnd.convertVnd(item.priceInput),
          image: item.pictures,
          categoryName: item.categoryName,
        };
      });
      this.dataTables = data1;
      this.spinner.hide();
    });
  }
  deleteProduct(id: any) {
    this._productService.deleteProduct(id).subscribe((res) => {
      this._notificationService.showSuccess(
        'Xóa sản phẩm thành công',
        'Thông báo'
      );
      this.initGenerate();
    });
  }

  searchProduct(nameProduct: any) {
 
    this._productService.findProductsByName(nameProduct).subscribe((res) => {  
      let data1 = res.body.data.map((item) => {
        return {
          id: item.id,
          name: item.name,
          price: this.convertVnd.convertVnd(item.price),
          priceInput: this.convertVnd.convertVnd(item.priceInput),
          image: item.pictures,
          categoryName: item.categoryName,
        };
      });
      this.dataTables = data1;
    });
  }
  keyup(value: any) {
    this.searchProduct(value)
  }
}

import { ChangeDetectorRef, Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { forkJoin } from 'rxjs';
import { NotificationService } from 'src/app/notification/notification.service';
import { CategoryService } from 'src/app/Services/category.service';
import { ProductService } from 'src/app/Services/product.service';
import { SupplierService } from 'src/app/Services/supplier.service';
import { CKEditor4 } from 'ckeditor4-angular/ckeditor';
@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.css']
})
export class ProductUpdateComponent implements OnInit {

  public Images: any = [];
  public isDisabled = false;
  public editorData = '';
  public dataListCategories: any;
  public dataListSuppliers: any;
  itemSelectData: any;
  productId:any;
  public componentEvents: string[] = [];
  constructor(
    private _supplierService: SupplierService,
    private _categoryService: CategoryService,
    private _productService: ProductService,
    private _notificationService: NotificationService,
    private ref: ChangeDetectorRef,
    private spinner: NgxSpinnerService,
    public fb: FormBuilder,
    private router:Router,
    private route:ActivatedRoute
  ) {
    this.productId = this.route.snapshot.paramMap.get('id');
  }


  myFormCreate1 = this.fb.group({
    id: ['', [Validators.required]],
    name: ['', [Validators.required]],
    price: ['', [Validators.required]],
    priceInput: ['', [Validators.required]],
    sale: ['', [Validators.required]],
    inventory: ['', [Validators.required]],
    insurance: ['', [Validators.required]],
    accessory: ['', [Validators.required]],
    sensor: ['', [Validators.required]],
    imageProcessor: ['', [Validators.required]],
    screen: ['', [Validators.required]],
    iso: ['', [Validators.required]],
    shutterSpeed: ['', [Validators.required]],
    categoryId: ['', [Validators.required]],
    supplierId: ['', [Validators.required]],
    description: ['', [Validators.required]],
  });
  myForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),

    file: new FormControl('', [Validators.required]),

    fileSource: new FormControl('', [Validators.required]),
  });
  ngOnInit(): void {
    this.Images = [];
    this.spinner.show();
    this.initData();
    this.getDataSelectDropbox();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.initData();
  }
  get f() {
    return this.myForm.controls;
  }
  async initData() {

   await this._productService.findProductsById(this.productId).subscribe(res=>{
      let productDetail = {...res.body.data, imageTitle: res.body.data.pictures.length> 0? res.body.data.pictures[0].image: null};
      this.myFormCreate1.patchValue({
        id: productDetail.id,
        name: productDetail.name,
        price: productDetail.price,
        priceInput: productDetail.priceInput,
        sale: productDetail.sale,
        inventory: productDetail.inventory,
        insurance: productDetail.insurance,
        accessory: productDetail.accessory,
        sensor: productDetail.sensor,
        imageProcessor: productDetail.imageProcessor,
        screen: productDetail.screen,
        iso: productDetail.iso,
        shutterSpeed: productDetail.shutterSpeed,
        categoryId: productDetail.categoryId,
        supplierId: productDetail.supplierId,
        pictures :productDetail.pictures,
        description:productDetail.description,
        
      });
      this.Images = productDetail?.pictures;
      this.itemSelectData = productDetail;
    });

    
  }

  onFileChange(event: any) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;

      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();
        reader.onload = (event: any) => {
          let imageObj = { image: event.target.result };
          this.Images.push(imageObj);
          this.myForm.patchValue({
            fileSource: this.Images,
          });
        };
        reader.readAsDataURL(event.target.files[i]);
      }
    }
  }
  handleDeleteImage(item) {
    this.Images.splice(item, 1);
  }

  async getDataSelectDropbox() {
    debugger
    const supplierServiceObserverble = await this._supplierService.getSuppliers();
    const categoryServiceObserverble =  await this._categoryService.getCategories();
    forkJoin([
      supplierServiceObserverble,
      categoryServiceObserverble,
    ]).subscribe((results) => {
      this.dataListCategories = results[1].body.data;
      this.dataListSuppliers = results[0].body.data;
        this.spinner.hide();
    });
  }

  handleSubmitCreateProduct() {
    let payload = {
      ...this.myFormCreate1.value,
      pictures: this.Images,
      description: this.editorData,
    };
    this._productService.updateProduct(payload).subscribe((item) => {
      this._notificationService.showSuccess("Cập nhật sản phẩm thành công", 'Thông báo');
      this.router.navigateByUrl('admin/product')
    });
  }

  public onChange(event: CKEditor4.EventInfo) {
    this.editorData = event.editor.getData();
  }

}

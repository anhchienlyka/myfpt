import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommentService } from 'src/app/Services/comment.service';
import { ConvertpriceService } from 'src/app/Services/convertprice.service';
import { ProductService } from 'src/app/Services/product.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css'],
})
export class ProductDetailComponent implements OnInit {
  productId: any;
  productDetail: any;
  coutComment: number = 0;
  daban : number =0;
  conlai:number = 0;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private commentService: CommentService,
    private productService: ProductService,
    private convertpriceService: ConvertpriceService

  ) {}

  ngOnInit(): void {
    this.productId = this.route.snapshot.paramMap.get('id');
    this.getCountComment();
    this.getProductByid(this.productId);
  }
  async getProductByid(id: any) {
    await this.productService.findProductsById(id).subscribe((res) => {
      let objectData = res.body.data;
      let data1 = {
        id: objectData.id,
        name: objectData.name,
        price: this.convertpriceService.convertVnd(objectData.price),
        priceInput: this.convertpriceService.convertVnd(objectData.priceInput),
        sale: objectData.sale,
        inventory: objectData.inventory,
        insurance: objectData.insurance,
        accessory: objectData.accessory,
        sensor: objectData.sensor,
        imageProcessor: objectData.imageProcessor,
        screen: objectData.screen,
        iso: objectData.iso,
        shutterSpeed: objectData.shutterSpeed,
        categoryName: objectData.category.categoryName,
        supplierName: objectData.supplier.companyName,
        pictures: objectData.pictures,
        description: objectData.description,
        coutSaleProduct: objectData.coutSaleProduct,
        imageTitle: objectData.pictures.length> 0? objectData.pictures[0].image: null
      
      };
      this.productDetail = data1;
      this.daban = this.productDetail.coutSaleProduct;
     this.conlai = this.productDetail.inventory -this.daban;
    });
  }
   getCountComment() {
   
     this.commentService
      .GetCoutCommentByProductId(this.productId)
      .subscribe((res) => {
        this.coutComment = res.body.data;
      });
  
  }
  editProduct(id: any) {
    this.router.navigateByUrl(`admin/product/update/${id}`);
  }
}

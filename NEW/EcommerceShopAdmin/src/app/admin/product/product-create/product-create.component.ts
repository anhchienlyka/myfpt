import {
  ChangeDetectorRef,
  Component,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationService } from 'src/app/notification/notification.service';
import { CategoryService } from 'src/app/Services/category.service';
import { ProductService } from 'src/app/Services/product.service';
import { SupplierService } from 'src/app/Services/supplier.service';
import { forkJoin } from 'rxjs';
import { Router } from '@angular/router';
import { ConvertimageService } from 'src/app/Services/convertimage.service';
import { CKEditor4 } from 'ckeditor4-angular/ckeditor';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css'],
})
export class ProductCreateComponent implements OnInit {
  ImagesList: any = [];
  public isDisabled = false;
  public editorData = '';
  public dataListCategories: any;
  public dataListSuppliers: any;
  itemSelectData: any;
  description: any;
  constructor(
    private _supplierService: SupplierService,
    private _categoryService: CategoryService,
    private _productService: ProductService,
    private _notificationService: NotificationService,
    private ref: ChangeDetectorRef,
    private spinner: NgxSpinnerService,
    private router: Router,
    public fb: FormBuilder,
    private convertimage: ConvertimageService
  ) {
    this.ImagesList = [];
  }

  ngOnInit(): void {
    this.spinner.show();
    this.initData();
    this.getDataSelectDropbox();
  }
  myFormCreate1 = this.fb.group({
    id: ['', [Validators.required]],
    name: ['', [Validators.required]],
    price: ['', [Validators.required]],
    priceInput: ['', [Validators.required]],
    sale: ['', [Validators.required]],
    inventory: ['', [Validators.required]],
    insurance: ['', [Validators.required]],
    accessory: ['', [Validators.required]],
    sensor: ['', [Validators.required]],
    imageProcessor: ['', [Validators.required]],
    screen: ['', [Validators.required]],
    iso: ['', [Validators.required]],
    shutterSpeed: ['', [Validators.required]],
    categoryId: ['', [Validators.required]],
    supplierId: ['', [Validators.required]],
  });
  myForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    file: new FormControl('', [Validators.required]),
    fileSource: new FormControl('', [Validators.required]),
  });

  ngOnChanges(changes: SimpleChanges): void {
    this.initData();
  }
  get f() {
    return this.myForm.controls;
  }
  initData() {}

  onFileChange(event: any) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();
        reader.onload = (event: any) => {
          let imageObj = { image: event.target.result };
          this.ImagesList.push(imageObj);
          this.myForm.patchValue({
            fileSource: this.ImagesList,
          });
        };
        reader.readAsDataURL(event.target.files[i]);
      }
    }
  }
  handleDeleteImage(item) {
    this.ImagesList.splice(item, 1);
  }
  async getDataSelectDropbox() {
    const supplierServiceObserverble = await this._supplierService.getSuppliers();
    const categoryServiceObserverble = await this._categoryService.getCategories();
    forkJoin([
      supplierServiceObserverble,
      categoryServiceObserverble,
    ]).subscribe((results) => {
      this.dataListCategories = results[1].body.data;
      this.dataListSuppliers = results[0].body.data;
      this.spinner.hide();
    });
  }
  handleSubmitCreateProduct() {
    let payload = {
      ...this.myFormCreate1.value,
      description: this.editorData,
      pictures: this.ImagesList,
    };
    this._productService.createProduct(payload).subscribe((item) => {
      this._notificationService.showSuccess(
        'Thêm sản phẩm thành công',
        'Thông báo'
      );
      this.router.navigateByUrl('admin/product');
    });
  }

  public onChange(event: CKEditor4.EventInfo) {
    this.editorData = event.editor.getData();
  }
  
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from 'src/app/Services/account.service';
import { MenuObjectJson } from '../common/menu-json';

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.css']
})
export class AdminLayoutComponent implements OnInit {
  public navItems: any
  userInfor : any
  constructor( private accountService:AccountService,   private route: Router) { }

  ngOnInit(): void {
    this.userInfor =  this.accountService.getCurrentUser();
  }

  handleLogount(){
    debugger
    this.accountService.logout();
    this.route.navigateByUrl('/login');
  }



}

import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/Services/customer.service';
import { Options } from '../common/interfaces/options';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  Options: Options;
  Headers: any[];
  dataTables: any = [];
  constructor(private customerService: CustomerService, private ref: ChangeDetectorRef,private datepipe: DatePipe) { }

  ngOnInit(): void {
    this.initGenerate();
  }
  async initGenerate(){
    this.customerService.getAllCustomer().subscribe(item => {
      let data1 = item.body.data.map(item => {
        return {
          fullName: item.fullName,
          userName:item.userName,   
          address:item.address,
          customerRank:this.rankCustomer(item.customerRank)
        };
      });
      this.dataTables = data1;
      this.ref.detectChanges();
    }) 
  }

   rankCustomer(rank :any):any{
    if(rank==2)return "Đồng";
    if(rank==3)return "Bạc";
    if(rank==4)return "Vàng";
    return "Chưa xếp hạng"
   }
}

import { Component, OnInit } from '@angular/core';
import { CommentService } from 'src/app/Services/comment.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  constructor(private commentService:CommentService) { }
  dataTables:any;
  ngOnInit(): void {
    this,this.initGenerate();
  }
  async initGenerate(){
    this.commentService.getAllComments().subscribe(item => {
      let data1 = item.body.data.map(item => {
        return {
          id: item.id,
          userId:item.userId,   
          productId: item.productId,
          commnentText:item.commnentText,   
          commnetTime:item.commnetTime,
        };
      });
      this.dataTables = data1;
    }) 
  }
}

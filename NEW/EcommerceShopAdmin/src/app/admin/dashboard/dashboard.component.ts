import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType  } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { ConvertpriceService } from 'src/app/Services/convertprice.service';
import { DrashbroadAdminService } from 'src/app/Services/drashbroad-admin.service';
import { OrderService } from 'src/app/Services/order.service';
import { ProductService } from 'src/app/Services/product.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  dataTypeMonth: any;
  month :any
  public BORDER_COLOR = '#00c9a7';
  public lineChartData: ChartDataSets[] = [
 
  ];
   public lineChartLabels: Label[] = [
   ];
  public lineChartOptions: ChartOptions  = {
    responsive: true,
  };
  productStock:any;
  topProduct:any
  public lineChartColors: Color[] = [];
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public lineChartPlugins = [];
 RevenueStatisticByDay : any;
 mycolorYear:any;
 mycolorMonth:any;
  constructor(private dashBroadAdminService : DrashbroadAdminService,private convertpriceService:ConvertpriceService,private orderService:OrderService,private productService:ProductService){ }
  ngOnInit(): void {
    this.initGenerate();
    this.getRevenueStatisticByDay();
    
  }
  handleBenefitMonth(option: any){
  this.mycolorMonth="#49cc90";
  this.mycolorYear="white"
    this.lineChartData = [];
    this.lineChartLabels = [];
    for(let i = 1; i<32; i++){
      this.lineChartLabels.push(i.toString());
    }
    this.dashBroadAdminService.getBenefitByMonthOrYear(option).subscribe(item=>{
      let body = item.body;
      if(body){
        this.lineChartColors = [];
        let objectDataChartTurnover = {data:[], label: 'Doanh số'};
        let objectDataChartProfit = {data:[], label: 'Lợi nhuận'};
        for(let i = 1 ; i< 32; i++){
          objectDataChartTurnover.data.push(0);
          objectDataChartProfit.data.push(0);   
        }
        for(let item of body){
            objectDataChartTurnover.data[parseInt(item.label) -1] = item.turnover;
            objectDataChartProfit.data[parseInt(item.label) -1] = item.profit ;       
        } 
        this.lineChartColors.push({
          borderColor: this.BORDER_COLOR,
        });
        this.lineChartData.push(objectDataChartTurnover);
        this.lineChartData.push(objectDataChartProfit);
        // console.log("this.lineChartData", this.lineChartData);  
      } 
    })
  }
  initGenerate(){
    let objectDataChartTurnover = {data:[], label: 'Doanh số'};
    let objectDataChartProfit = {data:[], label: 'Lợi nhuận'};
    this.lineChartData.push(objectDataChartTurnover);
    this.lineChartData.push(objectDataChartProfit);
    this.lineChartColors.push({
      borderColor: this.BORDER_COLOR,
    });
    this.getProductStock();
    this.getTopProductByMonth();
  }
  handleBenefitYear(option:any){
    this.mycolorYear="#49cc90";
    this.mycolorMonth="white"
    this.lineChartData = [];
    this.lineChartLabels = [];
    for(let i = 1; i<13; i++){
      this.lineChartLabels.push(i.toString());
    }
    this.dashBroadAdminService.getBenefitByMonthOrYear(option).subscribe(item=>{
      let body = item.body;
      if(body){
        this.lineChartColors = [];
        let objectDataChartTurnover = {data:[], label: 'Doanh số'};
        let objectDataChartProfit = {data:[], label: 'Lợi nhuận'};
        for(let i = 1 ; i< 13; i++){
          objectDataChartTurnover.data.push(0);
          objectDataChartProfit.data.push(0);   
        }
        for(let item of body){
            objectDataChartTurnover.data[parseInt(item.label) -1] = item.turnover;
            objectDataChartProfit.data[parseInt(item.label) -1] = item.profit ;       
        } 
        this.lineChartColors.push({
          borderColor: this.BORDER_COLOR,
        });
        this.lineChartData.push(objectDataChartTurnover);
        this.lineChartData.push(objectDataChartProfit);
        // console.log("this.lineChartData", this.lineChartData);  
      } 
    })

  }


getRevenueStatisticByDay(){
  this.orderService.getRevenueStatisticByDay().subscribe(res=>{
    let data1 = res.body.data;
    this.RevenueStatisticByDay = data1;
  })
}

getProductStock(){
  this.productService.getProductStock().subscribe(res=>{
    let data1 = res.body.data;
    this.productStock = data1;
  })
}

getTopProductByMonth(){
  this.orderService.getTopProductByMonth().subscribe(res=>{

    let data1 = res.body.data;
    this.topProduct = data1;
  })
}
}

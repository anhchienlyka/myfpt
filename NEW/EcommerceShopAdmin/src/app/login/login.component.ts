import { Component, EventEmitter, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationService } from '../notification/notification.service';
import { AccountService } from '../Services/account.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  userData:any;
  
  constructor(
    private accountService: AccountService,
    private route: Router,
    private notificationSevice: NotificationService,
  ) {
  }


public loginForm = new FormGroup({
    username:new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  })

  ngOnInit(): void {
  }


  onSubmitLogin() {
    
    let model: Login = this.loginForm.value;
    this.accountService.login(model).subscribe(res => {
      const obj = JSON.parse(res);
     if(obj.code==1)
     {
       this.userData = obj.data
       localStorage.setItem('userInforLogIn',JSON.stringify(this.userData));
       Login.callBack.emit();
        if(this.userData.roles==1){
        this.route.navigate(['/admin/dashboard']);
        this.notificationSevice.showSuccess("Đăng nhập thành công","Thông báo");
       }
       else{
        this.route.navigateByUrl('/error');
       }
     
     }
     else{
       this.route.navigateByUrl('/login');
       this.notificationSevice.showError("Đăng nhập không thành công","Thông báo");
     }
    });
    

  }
}
export class Login {
  static callBack = new EventEmitter();
}
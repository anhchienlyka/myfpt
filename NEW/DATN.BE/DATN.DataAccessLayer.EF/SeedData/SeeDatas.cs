﻿using DATN.Data.Entities;
using DATN.Data.Viewmodel.ProductViewModel;
using DATN.InfrastructureLayer.Enums;
using System.Collections.Generic;
using System.Linq;

namespace DATN.DataAccessLayer.EF.SeedData
{
    public class SeeDatas
    {
        private readonly DATNDBContex _contex;

        public SeeDatas(DATNDBContex contex)
        {
            _contex = contex;
        }

        public void Seed()
        {
            List<Category> categories = new List<Category>()
            {
                new Category(){ CategoryName = "Máy Ảnh",Active=true},
                new Category(){CategoryName = "Máy Quay",Active=true},
                new Category(){CategoryName = "Ống Kính",Active=true},
                new Category(){CategoryName = "Âm Thanh",Active=true},
                new Category(){CategoryName = "Phụ Kiện",Active=true},
                new Category(){CategoryName = "Đồng hồ",Active=true},
                new Category(){CategoryName = "Đồ Công Nghệ Khác",Active=true},
                new Category(){CategoryName = "Dịch Vụ Bảo hành",Active=true},
            };
            if (_contex.Categories.Count() == 0)
            {

                _contex.Categories.AddRange(categories);
            }

            List<Supplier> suppliers = new List<Supplier>()
            {
                new Supplier(){CompanyName="SAMSUNG", Address = "BAC NINH"},
                new Supplier(){CompanyName="CANON", Address = "BAC NINH"},
                new Supplier(){CompanyName="NOKIA", Address = "BAC NINH"},
                new Supplier(){CompanyName="LG", Address = "BAC NINH"},
                new Supplier(){CompanyName="SONY", Address = "BAC NINH"},
            };
            if (_contex.Suppliers.Count() == 0)
            {

                _contex.Suppliers.AddRange(suppliers);
            }

            List<Product> products = new List<Product>()
            {
                new Product()
                {
                    Name = "Máy Ảnh Canon Powershot",
                    Price = 20000000,
                    Sale = 15,
                    Inventory = 5,
                    Description = "Canon 6D Mark II Kit EF 24-105 F4L IS II USM  là chiếc máy ảnh cao cấp được nâng cấp từ người đàn anh Canon 6D đã được sản xuất từ năm 2012. Phiên bản mới được nâng cấp cảm biến ảnh full-frame với độ phân giải 26.2 MP tích hợp công nghệ lấy nét Dual Pixel cùng chip xử lý hình ảnh DIGIC 7. Canon cũng nâng cấp khả năng quay phim với chuẩn 1080p/60fps và tích hợp đầy đủ các kết nối Wi-Fi, Bluetooth, NFC và GPS vào trong một chiếc máy thuộc dòng cao cấp.",
                    Category = categories[1],
                    Supplier = suppliers[1],
                    Insurance= 12,
                    Accessory="Pin, Sạc, Bao da",
                    Sensor = "CCD",
                    PriceInput=10000000,
                    ImageProcessor="HacoLED",
                    Screen = "12",
                    ISO="ISO-9000",
                    CoutSaleProduct=0,
                    ShutterSpeed="1/250 giây"
                },
                new Product()
                {
                    Name = "Máy ảnh Canon EOS 9D",
                    Price = 18000000,
                    Sale = 10,
                    Inventory = 10,
                    Description = "Canon là một trong hai thương hiệu lớn nhất trên thị trường máy ảnh DSLR. Tuy nhiên, những sản phẩm của Canon lại vô cùng đa dạng, làm cho người mua gặp nhiều bối rối khi lựa chọn. Hãy cùng Điện máy XANH tìm hiểu các dòng máy ảnh cơ DSRL của Canon và đối tượng sử dụng nhé!",
                    Category = categories[0],
                    Supplier = suppliers[2],
                    Insurance= 12,
                    Accessory="Pin, Sạc, Bao da",
                    Sensor = "CCD",
                    ImageProcessor="HacoLED",
                    Screen = "12",
                    ISO = "9000",
                    CoutSaleProduct=0,
                    PriceInput=10000000,
                    ShutterSpeed="1/250 giây"
                },
                new Product()
                {
                    Name = "Máy Ảnh Canon EOS 6D",
                    Price = 15000000,
                    Sale = 0,
                    Inventory = 10,
                    Description = "Canon là một trong hai thương hiệu lớn nhất trên thị trường máy ảnh DSLR. Tuy nhiên, những sản phẩm của Canon lại vô cùng đa dạng, làm cho người mua gặp nhiều bối rối khi lựa chọn. Hãy cùng Điện máy XANH tìm hiểu các dòng máy ảnh cơ DSRL của Canon và đối tượng sử dụng nhé!",
                    Category = categories[1],
                    Supplier = suppliers[3],
                    Insurance= 12,
                    Accessory="Pin, Sạc, Bao da",
                    Sensor = "CMOS",
                    ImageProcessor="HacoLED",
                    Screen = "12",
                    CoutSaleProduct=0,
                      ISO="ISO-9000",
                    PriceInput=10000000,
                    ShutterSpeed="1/250 giây"
                },
                new Product()
                {
                    Name = "Máy Ảnh Canon EOS 5D",
                    Price = 30000000,
                    Sale = 15,
                    Inventory = 4,
                    Description = "Canon là một trong hai thương hiệu lớn nhất trên thị trường máy ảnh DSLR. Tuy nhiên, những sản phẩm của Canon lại vô cùng đa dạng, làm cho người mua gặp nhiều bối rối khi lựa chọn. Hãy cùng Điện máy XANH tìm hiểu các dòng máy ảnh cơ DSRL của Canon và đối tượng sử dụng nhé!",
                    Category = categories[0],
                    Supplier = suppliers[2],
                    Insurance= 12,
                    Accessory="Pin, Sạc, Bao da",
                    Sensor = "CMOS",
                    ImageProcessor="HacoLED",
                    Screen = "12",
                    CoutSaleProduct=0,
                      ISO="ISO-9000",
                    PriceInput=10000000,
                    ShutterSpeed="1/250 giây"
                },
                new Product()
                {
                    Name = "Máy Ảnh Fujifilm X-T38",
                    Price = 30000000,
                    Sale = 15,
                    Inventory = 4,
                    Description = "Canon là một trong hai thương hiệu lớn nhất trên thị trường máy ảnh DSLR. Tuy nhiên, những sản phẩm của Canon lại vô cùng đa dạng, làm cho người mua gặp nhiều bối rối khi lựa chọn. Hãy cùng Điện máy XANH tìm hiểu các dòng máy ảnh cơ DSRL của Canon và đối tượng sử dụng nhé!",
                    Category = categories[0],
                    Supplier = suppliers[1],
                    Insurance= 12,
                    Accessory="Pin, Sạc, Bao da",
                    Sensor = "CMOS",
                    ImageProcessor="HacoLED",
                    Screen = "12",
                      ISO="ISO-9000",
                      CoutSaleProduct=0,
                    PriceInput=10000000,
                    ShutterSpeed="1/250 giây"
                },
                new Product()
                {
                    Name = "Máy Ảnh Fujifilm X-T30",
                    Price = 30000000,
                    Sale = 25,
                    Inventory = 4,
                    Description = "Canon là một trong hai thương hiệu lớn nhất trên thị trường máy ảnh DSLR. Tuy nhiên, những sản phẩm của Canon lại vô cùng đa dạng, làm cho người mua gặp nhiều bối rối khi lựa chọn. Hãy cùng Điện máy XANH tìm hiểu các dòng máy ảnh cơ DSRL của Canon và đối tượng sử dụng nhé!",
                    Category = categories[0],
                    Supplier = suppliers[1],
                    Insurance= 12,
                    Accessory="Pin, Sạc, Bao da",
                    Sensor = "CMOS",
                    ImageProcessor="HacoLED",
                    Screen = "10",
                      ISO="ISO-9000",
                      CoutSaleProduct=0,
                    PriceInput=10000000,
                    ShutterSpeed="1/250 giây"
                },
                new Product()
                {
                    Name = "Máy Ảnh Canon EOS 90D Kit EF",
                    Price = 30000000,
                    Sale = 25,
                    Inventory = 4,
                    Description = "Canon là một trong hai thương hiệu lớn nhất trên thị trường máy ảnh DSLR. Tuy nhiên, những sản phẩm của Canon lại vô cùng đa dạng, làm cho người mua gặp nhiều bối rối khi lựa chọn. Hãy cùng Điện máy XANH tìm hiểu các dòng máy ảnh cơ DSRL của Canon và đối tượng sử dụng nhé!",
                    Category = categories[0],
                    Supplier = suppliers[1],
                    Insurance= 12,
                    Accessory="Pin, Sạc, Bao da",
                    Sensor = "CMOS",
                    ImageProcessor="HacoLED",
                    Screen = "12",
                    CoutSaleProduct=0,
                      ISO="ISO-9000",
                    PriceInput=10000000,
                    ShutterSpeed="1/250 giây"
                },
                new Product()
                {
                    Name = "Máy Ảnh Canon EOS 90D",
                    Price = 3000000,
                    Sale = 5,
                    Inventory = 4,
                    Description = "Canon là một trong hai thương hiệu lớn nhất trên thị trường máy ảnh DSLR. Tuy nhiên, những sản phẩm của Canon lại vô cùng đa dạng, làm cho người mua gặp nhiều bối rối khi lựa chọn. Hãy cùng Điện máy XANH tìm hiểu các dòng máy ảnh cơ DSRL của Canon và đối tượng sử dụng nhé!",
                    Category = categories[0],
                    Supplier = suppliers[1],
                    Insurance= 12,
                    Accessory="Pin, Sạc, Bao da",
                    Sensor = "Medium Format",
                    ImageProcessor="HacoLED",
                   Screen = "12",
                    CoutSaleProduct=0,
                      ISO="ISO-9000",
                    PriceInput=10000000,
                    ShutterSpeed="1/250 giây"
                },
                new Product()
                {
                    Name = "Máy Ảnh Canon EOS 90D",
                    Price = 30000000,
                    Sale = 10,
                    Inventory = 44,
                    Description = "Canon là một trong hai thương hiệu lớn nhất trên thị trường máy ảnh DSLR. Tuy nhiên, những sản phẩm của Canon lại vô cùng đa dạng, làm cho người mua gặp nhiều bối rối khi lựa chọn. Hãy cùng Điện máy XANH tìm hiểu các dòng máy ảnh cơ DSRL của Canon và đối tượng sử dụng nhé!",
                    Category = categories[0],
                    Supplier = suppliers[1],
                    Insurance= 12,
                    Accessory="Pin, Sạc, Bao da",
                    Sensor = "Medium Format",
                    ImageProcessor="HacoLED",
                   Screen = "12",
                      ISO="ISO-9000",
                      CoutSaleProduct=0,
                    PriceInput=10000000,
                    ShutterSpeed="1/250 giây"
                },
                new Product()
                {
                    Name = "Máy Ảnh Canon EOS 90D",
                    Price = 30000000,
                    Sale = 5,
                    Inventory = 41,
                    Description = "Canon là một trong hai thương hiệu lớn nhất trên thị trường máy ảnh DSLR. Tuy nhiên, những sản phẩm của Canon lại vô cùng đa dạng, làm cho người mua gặp nhiều bối rối khi lựa chọn. Hãy cùng Điện máy XANH tìm hiểu các dòng máy ảnh cơ DSRL của Canon và đối tượng sử dụng nhé!",
                    Category = categories[1],
                    Supplier = suppliers[2],
                    Insurance= 12,
                    CoutSaleProduct=0,
                    Accessory="Pin, Sạc, Bao da",
                    Sensor = "Medium Format",
                    ImageProcessor="HacoLED",
                    Screen = "12",
                      ISO="ISO-9000",
                    PriceInput=10000000,
                    ShutterSpeed="1/250 giây"
                },
                new Product()
                {
                    Name = "Máy Ảnh Canon EOS 90D",
                    Price = 30000000,
                    Sale = 5,
                    Inventory = 44,
                    Description = "Canon là một trong hai thương hiệu lớn nhất trên thị trường máy ảnh DSLR. Tuy nhiên, những sản phẩm của Canon lại vô cùng đa dạng, làm cho người mua gặp nhiều bối rối khi lựa chọn. Hãy cùng Điện máy XANH tìm hiểu các dòng máy ảnh cơ DSRL của Canon và đối tượng sử dụng nhé!",
                    Category = categories[2],
                    Supplier = suppliers[1],
                    Insurance= 12,
                    Accessory="Pin, Sạc, Bao da",
                    Sensor = "Medium Format",
                    ImageProcessor="HacoLED",
                        Screen = "12",
                    ISO="ISO-9000",
                    CoutSaleProduct=0,
                    PriceInput=10000000,
                    ShutterSpeed="1/250 giây",
                },
            };
            if (_contex.Products.Count() == 0)
            {

                _contex.Products.AddRange(products);
            }


            List<TopProductVM> listProductTop = new List<TopProductVM>()
            {
                new TopProductVM()
                {
                    Id=1,
                    Name="A",
                    CoutProductSale=3
                },
                 new TopProductVM()
                {
                    Id=2,
                    Name="B",
                    CoutProductSale=4
                },
                 new TopProductVM()
                {
                    Id=3,
                    Name="C",
                    CoutProductSale=5
                }
            };
            List<Payment> payments = new List<Payment>()
            {
                new Payment()
                {
                   PaymentType = PaymentType.CASH,
                   Allowed=true
                },
                new Payment()
                {
                   PaymentType = PaymentType.TRANSFER,
                   Allowed=true
                },
                new Payment()
                {
                   PaymentType = PaymentType.PAYPAL,
                   Allowed=true
                }
            };
            if (_contex.Payments.Count() == 0)
            {
                _contex.Payments.AddRange(payments);
            }
            var listUsers = new List<User>()
         {
                new User()
            {

                UserName = "admin",
                Password = "zaq12345",
                Roles = TypeRole.ADMIN
            },
                new User()
                {
                    FullName="Đinh Nho Sơn",
                    Address="Cầu Giấy, Hà Nội",
                    UserName="dinhnhoson",
                    CustomerRank= RANK.C,
                    Roles= TypeRole.CUSTOMER,
                    Password="zaq12345",
                    Phone="093728293",
                    Email="dinhnhoson@gmail.com",

                }
        };
            if (_contex.Users.Count() == 0)
            {
                _contex.Users.AddRange(listUsers);
            }
            _contex.SaveChanges();
        }
    }
}
﻿using DATN.Data.Entities;
using DATN.Data.Viewmodel.BenefitViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DATN.DataAccessLayer.EF.Interfaces
{
    public interface IOrderRepository
    {
        public Task<Order> GetOrderByOrderId(int id);

        public Task<IEnumerable<Order>> GetOrderByUserId(int userId);
        public Task<IEnumerable<Order>> GetAllOrder( );

        public Task<IEnumerable<Order>> GetOrderByDay();
        public Task<int> GetIdOrderMax();
        public Task<IEnumerable<Order>> GetListOrderByMonth();

    }
}
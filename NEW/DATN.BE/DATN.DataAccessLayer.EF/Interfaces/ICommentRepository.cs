﻿using DATN.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DATN.DataAccessLayer.EF.Interfaces
{
    public interface ICommentRepository
    {

        public Task<int> GetCoutCommentByProductId(int id);
        public Task<IEnumerable<Comment>> GetListCommentByProductId(int productId);
    }
}

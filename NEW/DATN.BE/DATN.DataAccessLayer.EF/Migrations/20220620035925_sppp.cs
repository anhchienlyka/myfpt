﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DATN.DataAccessLayer.EF.Migrations
{
    public partial class sppp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sp = @"CREATE PROCEDURE Pr_Get_Benefit(@option int)
                    as
                    begin
                    if @option=1
                    select DATEPART(DAY, CAST(o.CreateDate AS DATE))  as  'Label',sum(o.TotalCost) as 'Turnover', ( SUM(o.TotalCost) - SUM(p.PriceInput*od.Quantity)) as 'Profit' from OrderDetails as od
					join Orders as o On od.OrderId = o.Id
					left join Products as p On p.Id = od.ProductId 
                    where o.TransacStatus= 3 AND MONTH(o.CreateDate) = MONTH(GETDATE()) 
                    group by o.UserId, CAST(o.CreateDate AS DATE)
                    if @option=2
					select DATEPART(MONTH, CAST(o.CreateDate AS DATE))  as  'Label',sum(o.TotalCost) as 'Turnover', ( SUM(o.TotalCost) - SUM(p.PriceInput*od.Quantity)) as 'Profit' from OrderDetails as od
					join Orders as o On od.OrderId = o.Id
					left join Products as p On p.Id = od.ProductId 
                    where o.TransacStatus= 3 AND MONTH(o.CreateDate) = MONTH(GETDATE()) 
                    group by o.UserId, CAST(o.CreateDate AS DATE)
                    end";

            migrationBuilder.Sql(sp);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

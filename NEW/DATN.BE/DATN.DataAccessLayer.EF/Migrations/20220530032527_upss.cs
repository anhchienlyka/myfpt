﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DATN.DataAccessLayer.EF.Migrations
{
    public partial class upss : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ViewProduct",
                table: "Products");

            migrationBuilder.AddColumn<int>(
                name: "CoutSaleProduct",
                table: "Products",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CoutSaleProduct",
                table: "Products");

            migrationBuilder.AddColumn<int>(
                name: "ViewProduct",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}

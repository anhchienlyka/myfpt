﻿using DATN.Data.Entities;
using DATN.Data.Viewmodel.BenefitViewModel;
using DATN.DataAccessLayer.EF.Interfaces;
using DATN.InfrastructureLayer.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DATN.DataAccessLayer.EF.Implementations
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {

        public OrderRepository(DATNDBContex contex) : base(contex)
        {

        }

        public async Task<IEnumerable<Order>> GetAllOrder()
        {
            return await _dbContext.Orders.OrderByDescending(x => x.Id).ToListAsync();
        }

        public async Task<int> GetIdOrderMax()
        {
            var order = await _dbContext.Orders.OrderByDescending(x => x.Id).FirstOrDefaultAsync();
            if (order == null)
            {
                return 0;
            }
            return order.Id;
        }

        public async Task<IEnumerable<Order>> GetOrderByDay()
        {
 
            return await _dbContext.Orders.Include(k => k.OrderDetails).Where(x => x.CreateDate.Day == DateTime.Today.Day && x.TransacStatus == DeliveryStatus.SUCCESSFUL).ToListAsync();
        }

        public async Task<Order> GetOrderByOrderId(int id)
        {
            return await _dbContext.Orders.Include(x => x.User).Include(x => x.OrderDetails).Include(x => x.Payment).Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Order>> GetOrderByUserId(int userId)
        {
            return await _dbContext.Orders.Include(x => x.User).Include(x => x.Payment).Where(x => x.UserId == userId).OrderByDescending(x => x.Id).ToListAsync();
        }

        public async Task<IEnumerable<Order>> GetListOrderByMonth()
        {

            var listOrder = await _dbContext.Orders.Where(x => x.TransacStatus == DeliveryStatus.SUCCESSFUL && x.CreateDate.Month == DateTime.Now.Month).Include(x => x.OrderDetails).Take(5).ToListAsync();
            return listOrder;
        }
    }
}

﻿using DATN.Data.Entities;
using DATN.DataAccessLayer.EF.Interfaces;
using DATN.InfrastructureLayer.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DATN.DataAccessLayer.EF.Implementations
{
    public class UserRepository : IUserRepository
    {
        private readonly DATNDBContex _contex;
        public UserRepository(DATNDBContex contex)
        {
            _contex = contex;
        }

        public async Task<IEnumerable<User>> GetAllCustomers()
        {
            return await _contex.Users.Where(x => x.Roles == TypeRole.CUSTOMER).ToListAsync();
        }

      
        public async Task<User> GetUserByUserName(string userName)
        {
            return await _contex.Users.FirstOrDefaultAsync(x => x.UserName.Equals(userName));
        }
    }
}

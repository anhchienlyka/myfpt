﻿using DATN.Data.Entities;
using DATN.DataAccessLayer.EF.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DATN.DataAccessLayer.EF.Implementations
{
    public class CommentRepository : GenericRepository<Comment>,  ICommentRepository
    {
        public CommentRepository(DATNDBContex contex) : base(contex)
        {

        }

        public async Task<int> GetCoutCommentByProductId(int id)
        {
            var coutComment = await _dbContext.Comments.Where(x => x.Product.Id == id).ToListAsync();
            return coutComment.Count;
        }

        public async Task<IEnumerable<Comment>> GetListCommentByProductId(int productId)
        {
            return await _dbContext.Comments.Where(x => x.ProductId == productId).Include(x=>x.User).ToListAsync();
        }
    }
}

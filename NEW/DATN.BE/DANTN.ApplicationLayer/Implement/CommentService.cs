﻿using AutoMapper;
using DANTN.ApplicationLayer.Interface;
using DATN.Data;
using DATN.Data.Entities;
using DATN.Data.Viewmodel.CommentViewModel;
using DATN.DataAccessLayer.EF.Interfaces;
using DATN.DataAccessLayer.EF.UnitOfWorks;
using DATN.InfrastructureLayer.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DANTN.ApplicationLayer.Implement
{
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICommentRepository _commentRepository;
        public CommentService(IUnitOfWork unitOfWork, IMapper mapper,ICommentRepository commentRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _commentRepository = commentRepository;
        }

        public async Task<Response> Add(CommentAddVM comment)
        {
            var checkUser = await _unitOfWork.UserGenericRepository.GetAsync(comment.UserId);
           
            var checkProduct = await _unitOfWork.ProductGenericRepository.GetAsync(comment.ProductId);
            
            if (checkUser == null || checkProduct == null)
            {
                return new Response(SystemCode.Error, "Add comment Fail", null);
            }
            var data = _mapper.Map<Comment>(comment);
            await _unitOfWork.CommentGenericRepository.AddAsync(data);
            await _unitOfWork.CommitAsync();
            return new Response(SystemCode.Success, "Add Comment Success", data.Id);
        }

        public async Task<Response> Delete(int Id)
        {
            var data = await _unitOfWork.CommentGenericRepository.GetAsync(Id);
            if (data == null)
            {
                return new Response(SystemCode.Error, "Cannot find Comment", null);
            }
            _unitOfWork.CommentGenericRepository.Delete(data);
            await _unitOfWork.CommitAsync();
            return new Response(SystemCode.Success, "Delete Comment Success", data);
        }

        public async Task<Response> GetAll()
        {
            var data = await _unitOfWork.CommentGenericRepository.GetAllAsync();
            return new Response(SystemCode.Success, "Get all Comment Success", data);
        }

        public  async Task<Response> GetCoutCommentByProductId(int id)
        {
            var product = await _unitOfWork.ProductGenericRepository.GetAsync(id);
            if (product==null)
            {
                return new Response(SystemCode.Error, "Product is entity", null);
            }
            var data = await _commentRepository.GetCoutCommentByProductId(id);
            return new Response(SystemCode.Success, "Get Cout Comment Success", data);
        }

        public async Task<Response> GetListCommentByProductId(int productId)
        {
            var product = await _unitOfWork.ProductGenericRepository.GetAsync(productId);
            if (product == null)
            {
                return new Response(SystemCode.Error, "Product is entity", null);
            }
            var data = await _commentRepository.GetListCommentByProductId(productId);
            var listComment = new List<CommentVM>();
            foreach (var item in data)
            {
                var comemnt = new CommentVM()
                {
                    CommnentText = item.CommnentText,
                    CommnetTime = item.CommnetTime,
                    FullName = item.User.FullName,
                    ProductId = item.ProductId,
                    UserId = item.UserId

                };
                listComment.Add(comemnt);
            }
            return new Response(SystemCode.Success, "Get list Comemnt Success", listComment);
        }

        public async Task<Response> Update(CommentUpdateVM comment)
        {
            var data = await _unitOfWork.CommentGenericRepository.GetAsync(comment.Id);
            if (data == null)
            {
                return new Response(SystemCode.Error, "Cannot find Comment", null);
            }
            var dateNew = _mapper.Map<Comment>(comment);
            _unitOfWork.CommentGenericRepository.Update(dateNew);
            await _unitOfWork.CommitAsync();
            return new Response(SystemCode.Success, "Update Comment Success", data);
        }
    }
}
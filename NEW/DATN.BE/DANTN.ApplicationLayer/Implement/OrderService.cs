﻿using AutoMapper;
using DANTN.ApplicationLayer.Interface;
using DATN.Data;
using DATN.Data.Entities;
using DATN.Data.Viewmodel.BenefitViewModel;
using DATN.Data.Viewmodel.OrderDetailViewModel;
using DATN.Data.Viewmodel.OrderViewModel;
using DATN.Data.Viewmodel.ProductViewModel;
using DATN.DataAccessLayer.EF.Interfaces;
using DATN.DataAccessLayer.EF.UnitOfWorks;
using DATN.InfrastructureLayer.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DANTN.ApplicationLayer.Implement
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        private readonly IOrderRepository _orderRepository;
        private readonly IProductRepository _productRepository;
        public OrderService(IUnitOfWork unitOfWork, IMapper mapper, IOrderRepository orderRepository, IProductRepository productRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _productRepository = productRepository;
            _orderRepository = orderRepository;
        }

        public async Task<Response> Add(OrderAddVM order)
        {
            string dateNow = DateTime.Now.ToString("MM/dd/yyyy HH:mm");
            var user = await _unitOfWork.UserGenericRepository.GetAsync(order.UserId);
            if (user == null) return new Response(SystemCode.Error, "User not exits", null);
            var data = _mapper.Map<Order>(order);
            foreach (var item in data.OrderDetails)
            {
                var product = await _unitOfWork.ProductGenericRepository.GetAsync(item.ProductId);
                if (product == null) return new Response(SystemCode.Error, "Product is not exits", null);
                item.OrderId = data.Id;
                product.Inventory -= item.Quantity;
                product.CoutSaleProduct += item.Quantity;
                _unitOfWork.ProductGenericRepository.Update(product);
                await _unitOfWork.OrderDetailGenericRepository.AddAsync(item);
            }
            if (order.FullName == null && order.Phone == null && order.Address == null && order.Email == null)
            {
                data.FullName = user.FullName;
                data.Email = user.Email;
                data.Phone = user.Phone;
                data.Address = user.Address;
            }
            data.OrderNumber = data.OrderDetails.Count();
            data.TransacStatus = DeliveryStatus.PENDING;
            data.PaymentDate = DateTime.Parse(dateNow); 
            data.CreateDate = DateTime.Parse(dateNow);
            await _unitOfWork.OrderGenericRepository.AddAsync(data);
            await _unitOfWork.CommitAsync();
            return new Response(SystemCode.Success, "Add Order Success", data.Id);
        }

        public async Task<Response> CheckQuanityInCart(List<OrderDetailVM> listOrderDetail)
        {
            var listName = new List<string>();
            foreach (var item in listOrderDetail)
            {
                var product = await _unitOfWork.ProductGenericRepository.GetAsync(item.ProductId);
                if (product == null) return new Response(SystemCode.Error, "Product is not exits", null);
                if (item.Quantity > product.Inventory)
                {
                    listName.Add(product.Name);
                }
            }
            return new Response(SystemCode.Error, "fail", listName);
        }

        public Task<Response> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<Response> GetAll()
        {
            var data = await _orderRepository.GetAllOrder();
            var dataNew = new List<OrderVM>();
            foreach (var item in data)
            {
                var itemNew = _mapper.Map<OrderVM>(item);
                dataNew.Add(itemNew);
            }
            return new Response(SystemCode.Error, "Get all success", dataNew);
        }

        public async Task<Response> GetById(int id)
        {
            var data = await _orderRepository.GetOrderByOrderId(id);
            var dataNew = _mapper.Map<OrderVM>(data);
            if (dataNew == null)
            {
                return new Response(SystemCode.Error, "Order is null", null);
            }
            return new Response(SystemCode.Success, "Get Order Success", dataNew);
        }

        public async Task<Response> GetIdOrderMax()
        {
            var data = await _orderRepository.GetIdOrderMax();
            if (data == 0)
            {
                return new Response(SystemCode.Success, "Get Id Success", 0);
            }
            return new Response(SystemCode.Success, "Get Id Success", data);
        }

        public async Task<Response> GetOrderByUserId(int userId)
        {
            var data = await _orderRepository.GetOrderByUserId(userId);
            var listData = new List<OrderVM>();
            foreach (var item in data)
            {
                var dataNew = _mapper.Map<OrderVM>(item);
                listData.Add(dataNew);
            }
            if (listData == null)
            {
                return new Response(SystemCode.Error, "Order is null", null);
            }
            return new Response(SystemCode.Success, "Get Order Success", listData);
        }

        public async Task<Response> GetRevenueStatisticByDay()
        {

            var listOrders = await _orderRepository.GetOrderByDay();
            int coutOrder = listOrders.Count();
            decimal doanhthu = 0;
            decimal von = 0;
            foreach (var order in listOrders)
            {
                doanhthu += order.TotalCost;
                foreach (var item in order.OrderDetails)
                {
                    var product = await _unitOfWork.ProductGenericRepository.GetAsync(item.ProductId);
                    von = von + (product.PriceInput * item.Quantity);
                }
            }
            var RevenueStatistic = new BenefitDayVM()
            {
                CoutOrder = coutOrder,
                Turnover = doanhthu,
                Profit = doanhthu - von
            };
            return new Response(SystemCode.Success, "Get GetRevenueStatisticByDay Sucessful", RevenueStatistic);
        }

        public async Task<Response> GetTopProductByMonth()
        {

            var listTopProduct = new List<TopProductVM>();
            var listOrderData = await _orderRepository.GetListOrderByMonth();
            foreach (var item in listOrderData)
            {
            
                foreach (var detail in item.OrderDetails)
                {
                    var product = await _productRepository.GetProductById(detail.ProductId);
                    var listImages = await _productRepository.GetImageByProductId(detail.ProductId);
                    var image = listImages?.First();
                    var quanity = detail.Quantity;
                    var topProduct = new TopProductVM()
                    {
                        Id = product.Id,
                        Name = product.Name,
                        CoutProductSale = quanity,
                        Image = image.Image,
                        CategoryName= product.Category.CategoryName
                    };
                    if (listTopProduct.Any(x=>x.Id==product.Id)==true)
                    {
                        var productt = listTopProduct.Where(x => x.Id == product.Id).ToList().FirstOrDefault();
                        listTopProduct.Remove(productt);
                        productt.CoutProductSale += topProduct.CoutProductSale;
                        listTopProduct.Add(productt);
                    }
                    else
                    {
                        listTopProduct.Add(topProduct);
                    }
                }
            }
            return new Response(SystemCode.Success, "Get Top Product By Month Success ", listTopProduct);
        }
        public Task<Response> Update(OrderUpdateVM order)
        {
            throw new NotImplementedException();
        }

        public async Task<Response> UpdateStatusOrder(OrderStatusUpdateVM orderStatus)
        {
            var user = await _unitOfWork.UserGenericRepository.GetAsync(orderStatus.UserId);
            if (user == null)
            {
                return new Response(SystemCode.Error, "Can not find User", null);
            }
            if (user.IsDeleted == true)
            {
                return new Response(SystemCode.Error, "User is Deleted", null);
            }
            var data = await _orderRepository.GetOrderByOrderId(orderStatus.Id);

            if (user.Roles == TypeRole.CUSTOMER && orderStatus.TransacStatus == DeliveryStatus.DELIVERY)
            {
                data.TransacStatus = DeliveryStatus.SUCCESSFUL;

                var customer = await _unitOfWork.UserGenericRepository.GetAsync(data.UserId);
                customer.TotalPrice += (long)data.TotalCost;
                if (customer.TotalPrice >= 20000000) customer.CustomerRank = RANK.B;
                if (customer.TotalPrice >= 30000000) customer.CustomerRank = RANK.C;
                if (customer.TotalPrice >= 40000000) customer.CustomerRank = RANK.D;
                customer.CoutOrder++;
                _unitOfWork.UserGenericRepository.Update(customer);
                foreach (var item in data.OrderDetails)
                {
                    var product = await _unitOfWork.ProductGenericRepository.GetAsync(item.ProductId);
                    product.CoutSaleProduct += item.Quantity;
                    _unitOfWork.ProductGenericRepository.Update(product);
                }

            }
            if (user.Roles == TypeRole.ADMIN && orderStatus.TransacStatus == DeliveryStatus.PENDING)
            {
                data.TransacStatus = DeliveryStatus.DELIVERY;
            }
            if (user.Roles == TypeRole.ADMIN && orderStatus.TransacStatus == DeliveryStatus.DELIVERY)
            {
                data.TransacStatus = DeliveryStatus.SUCCESSFUL;

                var customer = await _unitOfWork.UserGenericRepository.GetAsync(data.UserId);
                customer.TotalPrice += (long)data.TotalCost;
                if (customer.TotalPrice >= 20000000) customer.CustomerRank = RANK.B;
                if (customer.TotalPrice >= 30000000) customer.CustomerRank = RANK.C;
                if (customer.TotalPrice >= 40000000) customer.CustomerRank = RANK.D;
                _unitOfWork.UserGenericRepository.Update(customer);
                foreach (var item in data.OrderDetails)
                {
                    var product = await _unitOfWork.ProductGenericRepository.GetAsync(item.ProductId);
                    product.CoutSaleProduct += item.Quantity;
                    _unitOfWork.ProductGenericRepository.Update(product);
                }
            }


            _unitOfWork.OrderGenericRepository.Update(data);
            await _unitOfWork.CommitAsync();
            return new Response(SystemCode.Success, "Update Status order succesfull", data.TransacStatus);
        }
    }
}
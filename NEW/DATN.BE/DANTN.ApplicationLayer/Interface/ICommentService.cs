﻿using DATN.Data;
using DATN.Data.Viewmodel.CommentViewModel;
using System.Threading.Tasks;

namespace DANTN.ApplicationLayer.Interface
{
    public interface ICommentService
    {
        public Task<Response> GetAll();

        public Task<Response> Add(CommentAddVM comment);

        public Task<Response> Update(CommentUpdateVM comment);
        public Task<Response> GetCoutCommentByProductId(int id);
        public Task<Response> GetListCommentByProductId(int productId);

        public Task<Response> Delete(int Id);
    }
}
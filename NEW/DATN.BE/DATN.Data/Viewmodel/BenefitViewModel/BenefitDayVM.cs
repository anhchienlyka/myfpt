﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DATN.Data.Viewmodel.BenefitViewModel
{
    public class BenefitDayVM
    {
        public int CoutOrder { get; set; }
        public decimal Turnover { get; set; }
        public decimal Profit { get; set; }
    }
}

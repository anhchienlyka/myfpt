﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DATN.Data.Viewmodel.PictureViewModel
{
   public class PictureVM
    {
        public int Id { get; set; }

        public int ProductId { get; set; }

        public string Image { get; set; }
    }
}

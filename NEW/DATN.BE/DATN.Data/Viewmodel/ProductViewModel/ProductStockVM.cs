﻿using DATN.Data.Entities;
using DATN.Data.Viewmodel.PictureViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DATN.Data.Viewmodel.ProductViewModel
{
    public class ProductStockVM
    {
  
        public string Name { get; set; }
        public int Inventory { get; set; }
        public string CategoryName { get; set; }
        public string SupplierName { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DATN.Data.Viewmodel.ProductViewModel
{
    public class ProductAdminVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal PriceInput { get; set; }  
        public string CategoryName { get; set; }
        public string SupplierName { get; set; }
        public string Pictures { get; set; }
    }
}

﻿using Dapper;
using DATN.Data.Viewmodel.BenefitViewModel;
using DATN.DataAccessLayer.EF;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace DATN.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BenefitController : ControllerBase
    {
        private  DATNDBContex _dATNDBContext;
        private readonly IDbConnection _dbConnection;
        public BenefitController(  DATNDBContex dATNDBContex, IDbConnection dbConnection)
        {
            this._dATNDBContext = dATNDBContex;
            _dbConnection = dbConnection;
        }
        [HttpGet]
        public async Task<IActionResult> GetBenefit(string option)
        {
            var parameters = new DynamicParameters();
            parameters.Add("option", option,DbType.Int32);
            var procedureName = "Pr_Get_Benefit";
            var response = await _dbConnection.QueryAsync<BenefitVM>(procedureName, parameters, commandType: CommandType.StoredProcedure);

            return Ok(response);

        }
        [HttpGet]
        public Task<IActionResult> GetBenefitByDay()
        {
            return null;
        }
    }
}

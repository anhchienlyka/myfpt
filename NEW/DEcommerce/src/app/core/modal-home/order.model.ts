import { OrderDetail } from "./orderDetail.model"



export interface Order{  
    userId: number,
    createDate: string,
    transacStatus: number,
    paymentId: number,
    orderDetails:OrderDetail[],
    fullName? : string;
    phone?:string;
    address?:string;
    orderCode?:string;
    email?:string;
    totalCost?:number
}

export interface OrderVms{
    orderDate: string,
    transacStatus: number,
    orderCode?:string;
    totalCost?:number
}


export interface OrderStatusUpdate{
    id?: number,
    userId?: number,
    transacStatus?: number
}
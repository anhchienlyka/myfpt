export interface User {
  id: number;
  userName: string;
  email: string;
  phone: string;
  role: string;
  fullName: string;
  address: string;
  customerRank: number;
  coutOrder:number,
  totalPrice:number
}

export interface Salecode {
  id: number;
  codeName: string;
  valueCode: number;
  isDelete: boolean;
  startDateCode: Date;
  endDateCode: Date;
}

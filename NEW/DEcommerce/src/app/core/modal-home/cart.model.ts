export interface ProductOrder{
    productId: number,
    productName: string,
    image: string,
    price: number,
    sale: number,
    quantity: number,
}

import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private apiUrl = environment.apiUrl;
  constructor(private httpClient: HttpClient) { }
//https://localhost:44311/api/Category/GetAllCategory
  getOrders():Observable<HttpResponse<any>>{
    var options = {
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      responseType:'text' as const
    };
    return this.httpClient.get<any>(this.apiUrl+'Order/GetAllOrder',{observe: 'response' });
  }

  getIdOrderMax():Observable<HttpResponse<any>>
  {
    var url = this.apiUrl + `Order/GetIdOrderMax`;
      return this.httpClient.get(url, {observe: 'response'});
  }
getOrderByUserId(id:any):Observable<HttpResponse<any>>
  {
    var url = this.apiUrl + `Order/GetOrderByUserId?id=${id}`;
      return this.httpClient.get(url, {observe: 'response'});
  }
  updateStatusOrder(data:any):Observable<HttpResponse<any>>{
    return this.httpClient.post(this.apiUrl+'Order/UpdateStatusOrder',data, {observe: 'response' })
  }

  getTopProductByMonth():Observable<HttpResponse<any>>{
    return this.httpClient.get(this.apiUrl+'Order/GetTopProductByMonth', {observe: 'response' })
  
  }
}

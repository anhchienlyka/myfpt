import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
import { ProductOrder } from '../modal-home/cart.model';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  public cartItemList: any = [];
  private apiUrl = environment.apiUrl;
  constructor(private httpClient: HttpClient) { }

  addToCart(productOrder: ProductOrder) {
    var listProductInCart = this.getProductInCart();
    if (listProductInCart == null) {
      listProductInCart = [];
      listProductInCart.push(productOrder);
      localStorage.setItem('wallme-cart', JSON.stringify(listProductInCart));
      return;
    }
    //If add a item already exist in wallme-cart, then add quantity 1
    var productExistInCart = listProductInCart.find(
      ({ productId }) => productId === productOrder.productId
    );
    if (!productExistInCart) {
      listProductInCart.push(productOrder);
      localStorage.setItem('wallme-cart', JSON.stringify(listProductInCart));
      return;
    }
    //If add a new item not exist in wallme-cart
    productExistInCart.quantity += productOrder.quantity;
    localStorage.setItem('wallme-cart', JSON.stringify(listProductInCart));
  }


  checkQuanityInCart(data:any):Observable<HttpResponse<any>>{
    return this.httpClient.post(this.apiUrl+'Order/CheckQuanityInCart',data, {observe: 'response' })
  }
  getProductInCart() {
    let data: ProductOrder[] = [];
    if (localStorage.getItem('wallme-cart') !== 'undefined') {
      data = JSON.parse(localStorage.getItem('wallme-cart'));
    }
    return data;
  }
  getPriceInCart() {
    let data: number;
    if (localStorage.getItem('priceincart') !== 'undefined') {
      data = parseInt(localStorage.getItem('priceincart'));
      return data;
    }
    data = 0;
    return data;
  }
  setPriceInCart(price: number) {
    localStorage.setItem('priceincart', JSON.stringify(price));
  }
  calculateTotalPrice() {
    var productsOrder = this.getProductInCart();
    let totalPrice = 0;
    if (productsOrder.length)
      productsOrder.forEach((product) => {
        totalPrice +=
          (product.price * product.quantity * (100 - product.sale)) / 100;
      });
    return totalPrice;
  }

  updateCart(productOrder: ProductOrder[]) {
    localStorage.setItem('wallme-cart', JSON.stringify(productOrder));
    // var listProductInCart = this.getProductInCart();
    // var productExistInCart = listProductInCart.find(({productId})=> productId === productOrder.productId);
    // //check if quantity = 0, remove from cart
    // if(productOrder.quantity == 0){
    //   listProductInCart = listProductInCart.filter(product=>product.productId !== productOrder.productId);
    //   localStorage.setItem('wallme-cart',JSON.stringify(listProductInCart));
    // }
    // productExistInCart.quantity = productOrder.quantity;
    // localStorage.setItem('wallme-cart',JSON.stringify(listProductInCart));
  }

  removeCartItem(product: any) {
    this.cartItemList = this.getProductInCart();
    var productExistInCart = this.cartItemList.find(
      ({ productId }) => productId === product.productId
    );
    this.cartItemList.map((a: any, index: any) => {
      if (productExistInCart.productId === a.productId) {
        this.cartItemList.splice(index, 1);
      }
    });
    localStorage.setItem('wallme-cart', JSON.stringify(this.cartItemList));
  }
}

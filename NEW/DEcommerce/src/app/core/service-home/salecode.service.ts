import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class SalecodeService {

  private apiUrl = environment.apiUrl;
  constructor(private httpClient: HttpClient) { }

//https://localhost:44311/api/SaleCode/GetCodeByCodeName?name=d
  getSaleCodeByCodeName (codeName:string):Observable<HttpResponse<any>>{
    var url = this.apiUrl + `SaleCode/GetCodeByCodeName?name=${codeName}`;
    return this.httpClient.get<any[]>(url, {observe: 'response'});
  }
  getAllSaleCode():Observable<HttpResponse<any>>{
    var options = {
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      responseType:'text' as const
    };
    return this.httpClient.get<any>(this.apiUrl+'SaleCode/GetSaleCodeName',{observe: 'response' });
  }

  addSaleCode(data :any):Observable<HttpResponse<any>>{
    return this.httpClient.post<any>(this.apiUrl+'',{observe:'response'})
  }
  deleteSaleCode(saleCodeName :any):Observable<HttpResponse<any>>{
    return this.httpClient.delete<any>(this.apiUrl+`SaleCode/DeleteSaleCode?saleCodeName=${saleCodeName}`,{observe:'response'})
  }
}

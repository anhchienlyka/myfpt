import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class CommentService {
  private apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  getAllCommentById(productId: any):Observable<HttpResponse<any>>{
    var options = {
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      responseType:'text' as const
    };
    return this.httpClient.get<any>(this.apiUrl+`Comment/GetListCommentByProductId?id=${productId}`,{observe: 'response' });
  }
  createComment(body: any): Observable<HttpResponse<any>> {
    return this.httpClient.post(this.apiUrl + 'Comment/AddComment', body, {
      observe: 'response',
    });
  }
}

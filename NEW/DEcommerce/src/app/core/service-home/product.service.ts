import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
// import { Product } from '../model/product.model';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private apiUrl = environment.apiUrl;
  constructor(private httpClient: HttpClient) {}

  getAllProducts(): Observable<HttpResponse<any>> {
    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      responseType: 'text' as const,
    };
    return this.httpClient.get<any>(this.apiUrl + 'Product/GetAllProductInHome', {
      observe: 'response',
    });
  }

  findProductsByName(name: string) {
    var url = this.apiUrl + `Product/GetProductByName?name=${name}`;
    return this.httpClient.get<any>(url, { observe: 'response' });
  }

  findProductsByCategoryId(id: any) {
    var url = this.apiUrl + `Product/GetProductByCategoryId?id=${id}`;
    return this.httpClient.get<any>(url, { observe: 'response' });
  }

  findProductsById(id: any): Observable<HttpResponse<any>> {
    var url = this.apiUrl + `Product/GetProductById?id=${id}`;
    return this.httpClient.get<any>(url, { observe: 'response' });
  }

  getFeaturedProducts(): Observable<HttpResponse<any>> {
    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      responseType: 'text' as const,
    };
    return this.httpClient.get<any>(
      this.apiUrl + 'Product/GetFeaturedProduct',
      { observe: 'response' }
    );
  }
  GetRecentProduct(): Observable<HttpResponse<any>> {
    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      responseType: 'text' as const,
    };
    return this.httpClient.get<any>(this.apiUrl + 'Product/GetRecentProduct', {
      observe: 'response',
    });
  }
  createProduct(body: any): Observable<HttpResponse<any>> {
    return this.httpClient.post(this.apiUrl + 'Product/AddProduct', body, {
      observe: 'response',
    });
  }
  deleteProduct(id: any): Observable<HttpResponse<any>> {
    return this.httpClient.delete(
      this.apiUrl + `Product/DeleteProduct?id=${id}`,
      { observe: 'response' }
    );
  }
  updateProduct(body: any): Observable<HttpResponse<any>> {
    return this.httpClient.post(this.apiUrl + 'Product/UpdateProduct', body, {
      observe: 'response',
    });
  }
}

import { Component, Input, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { CartService } from 'src/app/core/service-home/cart.service';

@Component({
  selector: 'app-product-template',
  templateUrl: './product-template.component.html',
  styleUrls: ['./product-template.component.css'],
})
export class ProductTemplateComponent implements OnInit {
  @Input() product!: any;
  coutProduct: number = 0;
  kho: any;
  productInCart: any;
  constructor(
    private messageService: NzMessageService,
    private cartService: CartService
  ) {}

  ngOnInit() {
    this.kho = this.product.inventory;

    this.productInCart = this.cartService.getProductInCart();
  }

  addToCart() {
    
    this.coutProduct++;
 if(this.productInCart!=null){
  for(let i = 0;i<this.productInCart.length;i++){
    if (
      this.product.id == this.productInCart[i].productId &&
      this.coutProduct+this.productInCart[i].quantity > this.kho
    ) {
      this.messageService.error('Sản phẩm trong kho không đủ');
      return;
    }
  };
 }
    if (this.coutProduct > this.kho) {
      this.messageService.error('Số lượng sản phẩm trong kho không đủ');
      return;
    }
    let data = {
      productId: this.product.id,
      productName: this.product.name,
      image: this.product.image,
      price: this.product.price,
      sale: this.product.sale,
      quantity: 1,
    };

    this.messageService.success(`Đã thêm ${this.product.name} vào giỏ hàng`);
    this.cartService.addToCart(data);
  }
}

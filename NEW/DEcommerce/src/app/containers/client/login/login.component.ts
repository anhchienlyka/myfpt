import { Component, EventEmitter, NgZone, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AccountService } from 'src/app/core/service-home/account.service';

import { AuthenticationService } from '../auth/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  formData: FormGroup;
  userData:any;

  constructor(
    private accountService: AccountService,
    private route: Router,
    private  fb: FormBuilder,
    private messageService: NzMessageService
  ) {

    this.formData = fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    })

  }


  ngOnInit() {

  }

  onSubmit() {
    let model: Login = this.formData.value;
    this.accountService.login(model).subscribe(res => {
      const obj = JSON.parse(res);
     if(obj.code==1)
     {
       this.userData = obj.data
       localStorage.setItem('userInfor',JSON.stringify(this.userData));
       Login.callBack.emit();
       if(this.userData.roles==2)
       {
        this.route.navigate(['']).then(()=>{
          this.messageService.success("Đăng nhập thành công");
          window.location.reload()
        
        });
        
       }

     }
     else{
       this.route.navigateByUrl('/dang-nhap');
       this.messageService.error("Đăng nhập thất bại")
     }
    });


  }



}
export class Login {
  static callBack = new EventEmitter();
}

import { Component, NgZone, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AccountService } from 'src/app/core/service-home/account.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  registerForm: FormGroup;
  userData: any;

  constructor(
    private accountService: AccountService,
    private route: Router,
    private messageService: NzMessageService,
    private  fb: FormBuilder
  ) {

    this.registerForm = fb.group({
      fullName: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      userName: ['', Validators.required],
      password: ['', Validators.required],
      address: ['', Validators.required],

   })
  }




  ngOnInit(): void {

  }

  onSubmit(){
    let formData = this.registerForm.value;
    formData.roles = 2;
    this.accountService.register(formData).subscribe(res => {
      const response = JSON.parse(res);
      if(response.code==1)
     {
       this.messageService.success("Đăng ký thành công");
       this.route.navigateByUrl('/dang-nhap');
     }
     else{
      //  this.route.navigateByUrl('/register');
       this.messageService.error("Đăng ký không thành công");
     }
    });
  }
}

import { Component, EventEmitter, NgZone, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { BehaviorSubject, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { ProductOrder } from 'src/app/core/modal-home/cart.model';
import { OrderDetail } from 'src/app/core/model/order-detail';
import { ProductAttribute } from 'src/app/core/model/product-attribute';
import { AccountService } from 'src/app/core/service-home/account.service';
import { CartService } from 'src/app/core/service-home/cart.service';
import { ProductService } from 'src/app/core/service-home/product.service';
import { SalecodeService } from 'src/app/core/service-home/salecode.service';
import { CustomerService } from 'src/app/core/service/customer.service';
import { OrderService } from 'src/app/core/service/order.service';
import { DataHelper } from 'src/app/core/util/data-helper';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
export class CartComponent implements OnInit {
  formData!: FormGroup;
  orderDetail: ProductOrder[] = [];
  nzLoading: boolean = false;
  valueCode: any;
  maKhuyenMai: string = '';
  currentUser: any;
  constructor(
    private orderService: OrderService,
    private customerService: CustomerService,
    private cartService: CartService,
    private messageService: NzMessageService,
    private formBuilder: FormBuilder,
    private ngZone: NgZone,
    private salecodeService: SalecodeService,
    private router: Router,
    private accountService: AccountService,
    private productService: ProductService
  ) {}

  ngOnInit() {
    this.formData = this.formBuilder.group({
      FullName: [{ value: '', disabled: true }, Validators.required],
      PhoneNumber: [null, Validators.required],
      Address: [null, Validators.required],
      Note: [null],
    });
    this.getCart();
    
    this.updateCart();
    //   this.currentUser = this.accountService.getCurrentUser();
  }



  getCart() {
    this.orderDetail = this.cartService.getProductInCart();
  }

  get tongTien(): number {
    let total: number = 0;
    total = this.cartService.calculateTotalPrice();
    return total;
  }

  get tongTienThanhToan(): number {
    let consts;
    if (this.tongTien == 0) {
      return 0;
    }
    if (this.valueCode > 0) {
      consts = this.tongTien - (this.valueCode * this.tongTien) / 100 + 30000;
      this.cartService.setPriceInCart(consts);
      return consts;
    }
    consts = this.tongTien + 30000;
    this.cartService.setPriceInCart(consts);
    return consts;
  }
  updateCart() {
    this.orderDetail = this.orderDetail?.filter((x) => x.quantity > 0);
    this.cartService.updateCart(this.orderDetail);
  }
  async handleCheckout() {
    debugger
    let userLogin = this.accountService.getCurrentUser();
    if(userLogin==null){
      this.messageService.error("Bạn vui lòng đăng nhập để thanh toán")
      // this.router.navigateByUrl('dang-nhap')
      return;
    }
    this.cartService.checkQuanityInCart(this.orderDetail).subscribe((res) => {
      let listName = res.body.data;
      let stringName: string;
      if (listName.length > 0) {
        stringName = listName.join(',');
        this.messageService.error(`Sản phẩm ${stringName} không đủ hàng`);
      } else {
        this.router.navigate(['cart/dat-hang', {maKhuyenMai: JSON.stringify({maKhuyenMai: this.maKhuyenMai})}]).then(() => {
          window.location.reload();
        });
      }
    });
  }
  addKhuyenMai(capoun) {
    let maKm: any;
    let dateTime = new Date();
    this.salecodeService
      .getSaleCodeByCodeName(capoun.value)
      .subscribe((res) => {
        maKm = res.body.data;

        if (maKm != null) {
          this.maKhuyenMai = maKm.codeName;
          this.valueCode = maKm.valueCode;
          this.messageService.success('Thêm mã khuyến mại thành công');
        } else {
          this.messageService.error(
            'Mã khuyến mại không tồng tại hoặc đã hết hạn'
          );
        }
      });
  }
  huyKhuyenMai() {
    this.maKhuyenMai = '';
    this.valueCode = 0;
  }
  delete(product) {
    this.cartService.removeCartItem(product);
     this.orderDetail = this.cartService.getProductInCart();
     //Cart.callBack.emit();
  }
  navigate(path: string): void {
    this.ngZone.run(() => this.router.navigateByUrl(path)).then();
  }
}
export class Cart {
  static callBack = new EventEmitter();
}

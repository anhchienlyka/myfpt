import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { NzMessageService } from 'ng-zorro-antd/message';
import { finalize } from 'rxjs/operators';
import { Order } from 'src/app/core/modal-home/order.model';
import { User } from 'src/app/core/modal-home/User.model';
import { Customer } from 'src/app/core/model/customer';

import { AccountService } from 'src/app/core/service-home/account.service';
import { OrderService } from 'src/app/core/service-home/order.service';



@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  formData!: FormGroup;
  formChangePassword!: FormGroup;
  profile!: User;
  nzLoading: boolean = false;
  orders: any = [];
  customerRank:any;
  dissCount:any
  constructor(
   
    private messageService: NzMessageService,
    private formBuilder: FormBuilder,
    private accountService:AccountService,
    private orderService:OrderService
  ) { }

  ngOnInit() {
    this.formData = this.formBuilder.group({
      Code: [{ value: '', disabled: true }, Validators.required],
      Email: [{ value: '', disabled: true }, Validators.required],
      FullName: [null, Validators.required],
      PhoneNumber: [null, Validators.required],
      Address: [null, Validators.required],
      Dob: [null],
      Gender: [null],
      timePicker: [null]
    });
    this.formChangePassword = this.formBuilder.group({
      OldPassword: [null, Validators.required],
      NewPassword: [null, Validators.required]
    });
    this.getProfile();
    this.getAllOrder();
  }

  getProfile() {
  let data= this.accountService.getCurrentUser();
  let idUser = data.id;
  this.accountService.getUserById(idUser).subscribe(res=>{
  
    this.profile=res.body.data;
    if (this.profile.customerRank == 1) {
      this.customerRank = 'Chưa xếp hạng';
      this.dissCount = 0;
    }
    if (this.profile.customerRank == 2) {
      this.customerRank = 'Hạng Đồng';
      this.dissCount = 3;
    }
    if (this.profile.customerRank == 3) {
      this.customerRank = 'Hạng Bạc';
      this.dissCount = 5;
    }
    if (this.profile.customerRank == 4) {
      this.customerRank = 'Hạng Vàng';
      this.dissCount = 7;
    }
  })

  }

  getOrders() {
    let data= this.accountService.getCurrentUser();
    let idUser = data.id;
    this.orderService.getOrderByUserId(idUser).subscribe(res=>{
      console.log("lisstOrder",res)
      this.orders = res.body.data
    })
  }
getAllOrder(){
  let data= this.accountService.getCurrentUser();
  let idUser = data.id;
  this.orderService.getOrderByUserId(idUser).subscribe((item) => {
    console.log("lissssstorder",item.body)
    let data1 = item.body.data.map((item) => {
      return {
        id: item.id,
        orderCode: item.orderCode,
        fullName: item.fullName,
        userId: item.userId,
        createDate: item.createDate,
        totalCost: item.totalCost,
        transacStatus: item.transacStatus,
        paymentId: item.paymentId,
      };
    });
    this.orders = data1;
  
  });
}
 
updateStatus(id: any, status: any) {
  let data= this.accountService.getCurrentUser();
  let idUser = data.id;
  let orderStatus = {
    id: id,
    userId: idUser,
    transacStatus: status,
  };
  this.orderService.updateStatusOrder(orderStatus).subscribe((res) => {  
    this.getAllOrder();
   if(res.body.data!=3){
    this.messageService.success("Cập nhật trạng thái thành công");
   }
  });
}
 
}

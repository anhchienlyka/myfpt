import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ProductOrder } from 'src/app/core/modal-home/cart.model';
import { Order } from 'src/app/core/modal-home/order.model';
import { OrderDetail } from 'src/app/core/modal-home/orderDetail.model';
import { User } from 'src/app/core/modal-home/User.model';
import { AccountService } from 'src/app/core/service-home/account.service';
import { CartService } from 'src/app/core/service-home/cart.service';
import { CheckoutService } from 'src/app/core/service-home/checkout.service';
import { OrderService } from 'src/app/core/service-home/order.service';
import { ProductService } from 'src/app/core/service-home/product.service';
import { SalecodeService } from 'src/app/core/service-home/salecode.service';
import { Cart } from '../cart/cart.component';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css'],
})
export class CheckoutComponent implements OnInit {
  checked: any = true;
  checkBlock?: boolean = false;
  currentUser?: User;
  customerRank: string;
  dissCount?: number = 0;
  paymentType: number;
  maDH: string;
  radioValue: any;
  thanhTien: number;
  phuongthucthanhtoan: number;
  orderDetailsInOrder: OrderDetail[] = [];
  productsInCart: ProductOrder[];
  userData: User;
  maGiamGia:any
  daibcu:boolean
  tienThanhToanCuoiCung: number = 0;
  constructor(
    private accountService: AccountService,
    public fb: FormBuilder,
    private productService: ProductService,
    private messageService: NzMessageService,
    private cartService: CartService,
    private route: Router,
    private checkoutService: CheckoutService,
    private orderService: OrderService,
    private router: ActivatedRoute,
    private saleCodeService:SalecodeService
  ) {}

  currentUserForm = this.fb.group({
    fullName: [{value:'',disabled: true}],
    userName: [{value:'',disabled: true}],
    address: [{value:'',disabled: true}],
    phone: [{value:'',disabled: true}],
    email: [{value:'',disabled: true}],
  });
  currentUserForm2 = this.fb.group({
    fullName1: ['', [Validators.required]],
    address1: ['', [Validators.required]],
    phone1: ['', [Validators.required]],
    email1: ['', [Validators.required]],
  });

  ngOnInit(): void {
    this.maGiamGia = JSON.parse(this.router.snapshot.paramMap.get('maKhuyenMai')).maKhuyenMai;
    //console.log("this.maGiamGia", this.maGiamGia);
    this.initData();
  }
  myFunction() {
    this.checkBlock = !this.checkBlock;
  }
  handleChanger() {}
  initData() {
    this.getCurrentUser();
    this.checkoutAccount();
    this.getMaHoaDon();
    this.productsInCart = this.cartService.getProductInCart();
    
  }

  transfer() {
    
    for (let i = 0; i < this.productsInCart.length; i++) {
      let object1: OrderDetail = {
        orderId: 0,
        productId: 0,
        price: 0,
        total_Price: 0,
        quantity: 0,
      };
      object1.productId = this.productsInCart[i].productId;
      object1.price = this.productsInCart[i].price;
      object1.total_Price = this.tienThanhToanCuoiCung;
      object1.quantity = this.productsInCart[i].quantity;
      this.orderDetailsInOrder.push(object1);
    }
  }
  onSubmit() {
    if (this.phuongthucthanhtoan != 1 && this.phuongthucthanhtoan != 2) {
      this.messageService.warning('Vui lòng chọn phương thức thanh toán');
      return;
    }
    if (this.checkBlock == true) {
      this.checkOut2();

    } else {
      this.checkOut1();
    }
  }
  checkOut1() {
    let payload: any = {
      userId: this.currentUser.id,
      orderDetails: this.orderDetailsInOrder,
      orderNumber: this.orderDetailsInOrder.length,
      transacStatus: 1,
      orderCode: this.maDH,
      paymentId: this.paymentType,
      totalCost: this.tienThanhToanCuoiCung,
      createDate: new Date()
    };
    this.checkoutService.addOrder(payload).subscribe();
    if(this.maGiamGia!=null){this.saleCodeService.deleteSaleCode(this.maGiamGia).subscribe()}
    localStorage.removeItem('wallme-cart');
    localStorage.removeItem('priceincart');
    this.route.navigateByUrl('/dat-hang-thanh-cong');
  }
  checkOut2() {
    let payload: any = {
      userId: this.currentUser.id,
      orderDetails: this.orderDetailsInOrder,
      transacStatus: 1,
      orderCode: this.maDH,
      paymentId: this.paymentType,
      fullName: this.fullName1.value,
      email: this.email1.value,
      phone: this.phone1.value,
      address: this.address1.value,
      totalCost: this.tienThanhToanCuoiCung,
     
    };
    this.checkoutService.addOrder(payload).subscribe();
    if(this.maGiamGia!=null){this.saleCodeService.deleteSaleCode(this.maGiamGia).subscribe()}
    window.localStorage.removeItem('wallme-cart');
    window.localStorage.removeItem('priceincart');
    Cart.callBack.emit();
    this.route.navigateByUrl('/dat-hang-thanh-cong');
  }
  get fullName1() {
    return this.currentUserForm2.get('fullName1');
  }
  get userName1() {
    return this.currentUserForm2.get('userName1');
  }
  get address1() {
    return this.currentUserForm2.get('address1');
  }

  get phone1() {
    return this.currentUserForm2.get('phone1');
  }

  get email1() {
    return this.currentUserForm2.get('email1');
  }

  getMaHoaDon() {
    this.orderService.getIdOrderMax().subscribe((res) => {
      let dataId: number = res.body.data;
      dataId = dataId + 1;
      this.maDH = 'MDH' + dataId;
    });
  }
  getCurrentUser() {
  
    this.userData = this.accountService.getCurrentUser();
    let idUser = this.userData.id;
    let pricess: number = this.cartService.getPriceInCart();
    this.thanhTien = pricess;
    this.accountService.getUserById(idUser).subscribe((res) => {
      this.currentUser = res.body.data;
      if (this.currentUser.customerRank == 1) {
        this.customerRank = 'Chưa xếp hạng';
        this.tienThanhToanCuoiCung = pricess;
        this.dissCount = 0;
        this.transfer();
      }
      if (this.currentUser.customerRank == 2) {
        this.customerRank = 'Hạng Đồng';
        this.dissCount = 3;
        this.tienThanhToanCuoiCung = pricess - (pricess * this.dissCount) / 100;
        this.transfer();
      }
      if (this.currentUser.customerRank == 3) {
        this.customerRank = 'Hạng Bạc';
        this.dissCount = 5;
        this.tienThanhToanCuoiCung = pricess - (pricess * this.dissCount) / 100;
        this.transfer();
      }
      if (this.currentUser.customerRank == 4) {
        this.customerRank = 'Hạng Vàng';
        this.dissCount = 7;
        this.tienThanhToanCuoiCung = pricess - (pricess * this.dissCount) / 100;
        this.transfer();
      }
    });
  }
  checkoutAccount() {
    this.currentUserForm.patchValue({
      fullName: this.userData.fullName,
      userName: this.userData.userName,
      address: this.userData.address,
      phone: this.userData.phone,
      email: this.userData.email,
    });
  }

  radioForm = new FormGroup({
    radiostatus: new FormControl(''),
    remark: new FormControl(''),
  });

  onChangeStatus($event) {
    if ($event == 'passed') {
      this.paymentType = 1;
      this.phuongthucthanhtoan = 1;
    } else if ($event == 'failed') {
      this.paymentType = 2;
      this.phuongthucthanhtoan = 2;
    }
  }
}

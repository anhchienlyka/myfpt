import { ViewportScroller } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { Menu } from 'src/app/core/model/menu';
import { ProductService } from 'src/app/core/service-home/product.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  prolectByCategory: any = [];
  nzLoading: boolean = false;
  categoryId :any;
  filter = {
    menuAlias: "",
    orderBy: "highlight",
    price: "all",
    take: 20
  }

  constructor(
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private viewportScroller: ViewportScroller,
    private router: Router
  ) {
    this.router.events.forEach((event) => {
      if (event instanceof NavigationEnd) {
        this.categoryId = this.activatedRoute.snapshot.params.id;
        this.filter.take = 20;
        this.getData();
      }
    });
  }

  ngOnInit() {
    this.categoryId = this.activatedRoute.snapshot.params.id;
    this.getData();
  }

  getData() {
   
    this.productService.findProductsByCategoryId(this.categoryId)
      .subscribe((resp: any) => {
        this.prolectByCategory = resp.body?.data.map((item: any)=>{
          return {
            id: item.id,
            name: item.name,
            price: item.price,
            image: item.pictures?.length > 0 ? item.pictures[0].image: null,
            sale: item?.sale,

          }
        });
      }, error => {

      })
  }

  showMore() {
    let currentLocation: [number, number] = this.viewportScroller.getScrollPosition();
    this.filter.take += 20;
    this.nzLoading = true;
    this.productService.findProductsByCategoryId(this.filter.menuAlias)
      .pipe(
        finalize(() => {
          this.nzLoading = false;
        })
      )
      .subscribe((resp: any) => {
        this.prolectByCategory = resp.body?.data.map((item: any)=>{
          return {
            id: item.id,
            name: item.name,
            price: item.price,
            image: item.pictures.length > 0 ? item.pictures[0].image: null,
            sale: item.sale,
          }
        });
        console.log("this.prolectByCategory", this.prolectByCategory);
        setTimeout(() => {
          this.viewportScroller.scrollToPosition(currentLocation)
        }, 10);
      }, error => {

      })
  }
}

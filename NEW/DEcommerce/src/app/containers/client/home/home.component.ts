import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/core/model/article';
import { Gallery } from 'src/app/core/model/gallery';
import { Menu } from 'src/app/core/model/menu';
import { Product } from 'src/app/core/model/product';
import { ProductService } from 'src/app/core/service-home/product.service';
import { ArticleService } from 'src/app/core/service/article.service';
import { GalleryService } from 'src/app/core/service/gallery.service';
import { MenuService } from 'src/app/core/service/menu.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  slideConfig = {
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    dots: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 360,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  mainBanner: any = [];
  productSellings: Product[] = [];
  productAll: any = [];
  featuredProducts: any = [];
  recentProducts: any = [];
  menuHomePages: Menu[] = [];
  highlightArticle: Article[] = [];

  constructor(
    private menuService: MenuService,
    private articleService: ArticleService,
    private galleryService: GalleryService,
    // private productService: ProductService,
    private productService: ProductService
  ) { }

  ngOnInit() {
    this.getBanner();
    this.getProductSelling();
    this.getAllMenuHomePage();
    this.getHighlightArticle();
    this.getProductAll();
    this.getFeaturedProducts();
    this.getRecentProduct();
  }

  getBanner() {
    this.mainBanner =
    [
      {Image:"../../../../assets/imgs/slide/a1.jpg"},
      {Image:"../../../../assets/imgs/slide/a2.png"},
      {Image:"../../../../assets/imgs/slide/a3.png"},
      {Image:"../../../../assets/imgs/slide/a1.jpg"},
      {Image:"../../../../assets/imgs/slide/a2.png"},
      {Image:"../../../../assets/imgs/slide/a3.png"},
    ]
    // this.galleryService.get({})
    //   .subscribe((resp: any) => {
    //     let datas: Gallery[] = JSON.parse(resp["data"]);
    //     this.mainBanner = datas.filter(x => x.Type == 1);
    //   }, error => {

    //   })
  }

  getProductAll(){
      this.productService.getAllProducts()
      .subscribe((resp: any) => {
        // console.log("resp1234", resp.body?.data)
        this.productAll = resp.body?.data.map((item: any)=>{
          return {
            id: item.id,
            name: item.name,
            price: item.price,
            image: item.pictures.length > 0 ? item.pictures[0].image: null,
            sale: item.sale,
            inventory:item.inventory
          }
        });
      }, error => {

      })
  }
  getFeaturedProducts(){
    this.productService.getFeaturedProducts()
      .subscribe((resp: any) => {
        // console.log("resp1234", resp.body?.data)
        this.featuredProducts = resp.body?.data.map((item: any)=>{
          return {
            id: item.id,
            name: item.name,
            price: item.price,
            image: item.pictures.length > 0 ? item.pictures[0].image: null,
            sale: item.sale,
            inventory:item.inventory
          }
        });
      }, error => {

      })
  }

  getRecentProduct(){
    this.productService.GetRecentProduct()
    .subscribe((resp: any) => {
      // console.log("resp1234", resp.body?.data)
      this.recentProducts = resp.body?.data.map((item: any)=>{
        return {
          id: item.id,
          name: item.name,
          price: item.price,
          image: item.pictures.length > 0 ? item.pictures[0].image: null,
          sale: item.sale,
          inventory:item.inventory
        }
      });
    }, error => {

    })

  }

  getProductSelling() {
    // this.productService.getAllProducts()
    //   .subscribe((resp: any) => {
    //     console.log("resp", resp.body?.data)
    //     this.productAll = resp.body?.data;
    //   }, error => {

    //   })
  }

  getAllMenuHomePage() {
    // this.menuService.getAllMenuHomePage()
    //   .subscribe((resp: any) => {
    //     this.menuHomePages = JSON.parse(resp["data"]);
    //   }, error => {

    //   })
  }

  getHighlightArticle() {
    // this.articleService.getHighlight()
    //   .subscribe((resp: any) => {
    //     this.highlightArticle = JSON.parse(resp["data"])
    //   }, error => {

    //   })
  }
}

import { ChangeDetectionStrategy, ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { isTemplateRef } from 'ng-zorro-antd/core/util';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Product } from 'src/app/core/model/product';
import { ProductAttribute } from 'src/app/core/model/product-attribute';
import { CartService } from 'src/app/core/service-home/cart.service';
import { CommentService } from 'src/app/core/service-home/comment.service';
import { ConvertpriceService } from 'src/app/core/service-home/convertprice.service';
import { ProductService } from 'src/app/core/service-home/product.service';
import { OrderService } from 'src/app/core/service/order.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css'],
})
export class ProductDetailComponent implements OnInit {
  productId: any = '';
  productInCart:any;
  product!: any;
  qty: number = 1;
  image: string = './assets/imgs/tivi.png';
  likes = 0;
  dislikes = 0;
  time = new Date();
  data: any[] = [];
  submitting = false;
  listComment: any ;
coutProductInCart:number=0;
conlai:number =0;
  user = {
    author: 'Han Solo',
    avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png'
  };
  inputValue = '';

  constructor(
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private cartService: CartService,
    private messageService: NzMessageService,
    private ngZone: NgZone,
    private router: Router,
    private commentService: CommentService,
    private convertPriceService: ConvertpriceService,
    private changeDetectorRef: ChangeDetectorRef,

  ) {
    this.router.events.forEach((event) => {
      if (event instanceof NavigationEnd) {
        this.productId = this.activatedRoute.snapshot.params.id;
        this.getData();
      }
    });
  }

  ngOnInit() {
    this.productId = this.activatedRoute.snapshot.params.id;
    this.getData();
    this.productInCart= this.cartService.getProductInCart();
    this.getAllCommentByProduct();
    this.data.push({...this.user,content: "bcdai code",
      datetime: new Date(),
      displayTime: new Date()})
  }

  getData() {
    let aa:any;
    this.productService.findProductsById(this.productId).subscribe(
      (resp: any) => {
      //  console.log('resp', resp);
        let data = resp.body?.data;
        this.product = {
          id: data.id,
          name: data.name,
          oldPrice: data.price,
          sale: data.sale,
          images: data.pictures,
          image: data.pictures.length > 0 ? data.pictures[0].image : null,
          description: data.description,
          newPrice: (data.price * (100 - data.sale)) / 100,
          categoryName: data.category.categoryName,
          companyName: data.supplier.companyName,
          insurance: data.insurance,
          accessory: data.accessory,
          screen: data.screen,
          kho : data.inventory
        };
        this.conlai = this.product.kho
      //  console.log('resp', this.product);
      },
      (error) => {}
    );
  
  }

  showImg(src: string) {
  
    this.product.image = src;
  }

  chooseAttribute(attributes: ProductAttribute[], index: number) {
    for (let i = 0; i < attributes.length; i++) {
      if (i == index) {
        if (attributes[i].Checked == true) attributes[i].Checked = false;
        else attributes[i].Checked = true;
      } else attributes[i].Checked = false;
    }
  }

  addToCart() {
    this.coutProductInCart++;
 if(this.productInCart!=null){
  for(let i = 0;i<this.productInCart.length;i++){
    if (
      this.product.id == this.productInCart[i].productId &&
      this.coutProductInCart+this.productInCart[i].quantity > this.conlai
    ) {
      this.messageService.error('Sản phẩm trong kho không đủ');
      return;
    }
  };
 }
    if(this.coutProductInCart>this.conlai){
    this.messageService.error("Sản phẩm trong kho không đủ");
    return;
    }
    let data = {
      productId: this.product.id,
      productName: this.product.name,
      image: this.product.image,
      price: this.product.newPrice,
      sale: this.product.sale,
      quantity: 1,
    };
   // console.log('Dataaaaa', data);
    this.messageService.success(`Đã thêm ${this.product.name} vào giỏ hàng`);
    this.cartService.addToCart(data);
  }

  buyNow() {
    if (this.product.Attributes != null && this.product.Attributes.length > 0) {
      if (
        !(
          this.product.Attributes.findIndex(
            (x: { ProductAttributes: any[] }) =>
              x.ProductAttributes.findIndex((y) => y.Checked) >= 0
          ) >= 0
        )
      ) {
        this.messageService.error('Chọn ít nhất một thuộc tính sản phẩm');
        return;
      }
    }

    this.cartService.addToCart(this.product);
    this.navigate('/gio-hang');
  }
  like(): void {
    this.likes = 1;
    this.dislikes = 0;
  }

  dislike(): void {
    this.likes = 0;
    this.dislikes = 1;
  }
  getAllCommentByProduct(){
    this.commentService.getAllCommentById(this.productId)
      .subscribe((resp: any) => {
        setTimeout( () => {
          this.listComment = resp.body?.data;
      
        }, 0);   
      }, error => {

      })
  }
  handleSubmit(): void {
    debugger
    this.submitting = true;
    const content = this.inputValue;
    this.inputValue = '';
    if(localStorage.getItem('userInfor') !== null){
      let body = {
        userId: JSON.parse(localStorage.getItem('userInfor')).id,
        productId: this.productId,
        commnentText: content,
        commnetTime: new Date()
      }
      this.commentService.createComment(body).subscribe(res=>{
        this.getAllCommentByProduct();
      }) 

    }
    else{
      this.messageService.warning("Vui lòng đăng nhập !!!")
    }
    
  }
  
  


  navigate(path: string): void {
    this.ngZone.run(() => this.router.navigateByUrl(path)).then();
  }
}



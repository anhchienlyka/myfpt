import { ViewportScroller } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { Product } from 'src/app/core/modal-home/product.model';

import { ProductService } from 'src/app/core/service-home/product.service';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  products: Product[] = [];
  nzLoading: boolean = false;

  filter = {
    keySearch: "",
    orderBy: "highlight",
    price: "all",
    take: 4
  }

  constructor(
    private productService: ProductService,
    private viewportScroller: ViewportScroller,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.router.events.forEach((event) => {
      if (event instanceof NavigationEnd) {
        this.filter.keySearch = this.activatedRoute.snapshot.params.alias;
      
        this.filter.take = 4;
        this.getData();
      }
    });
  }

  ngOnInit() {
    this.filter.keySearch = this.activatedRoute.snapshot.params.alias;
    this.getData();
  }

  getData() {
    this.productService.findProductsByName(this.filter.keySearch)
      .subscribe((resp: any) => {
        this.products = resp.body?.data.map((item: any)=>{
          return {
            id: item.id,
            name: item.name,
            price: item.price,
            image: item.pictures.length > 0 ? item.pictures[0].image: null,
            sale: item.sale,

          }
        });
      }, error => {

      })
  }

  showMore() {
    let currentLocation: [number, number] = this.viewportScroller.getScrollPosition();
    this.filter.take += 4;
    this.nzLoading = true;
    this.productService.findProductsByName(this.filter.keySearch)
      .pipe(
        finalize(() => {
          this.nzLoading = false;
        })
      )
      .subscribe((resp: any) => {
        this.products = resp.body.data;
        setTimeout(() => {
          this.viewportScroller.scrollToPosition(currentLocation)
        }, 10);
      }, error => {

      })
  }
}
